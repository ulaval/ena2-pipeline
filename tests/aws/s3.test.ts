import { config } from 'https://deno.land/x/dotenv@v3.2.0/mod.ts';
import { AwsConfig } from '../../src/aws/awsconfig.ts';
import * as s3 from '../../src/aws/s3.ts';


Deno.test('Copie un fichier dans un bucket s3 aws', async () => {
    const config: AwsConfig = awsConfig();
    //On doit changer le répertoire de travail pour celui du script
    const scriptDir = new URL('.', import.meta.url).pathname;
    Deno.chdir(scriptDir); // Change le workdir

    await s3.copyToBucket(
        config,
        'ena2-dev-lambdas-bucket',
        'bidon.txt',
    );
});

function awsConfig() {
    config({ export: true });
    const accessKeyId = Deno.env.get('AWS_ACCESS_KEY_ID');
    const accessKeySecret = Deno.env.get('AWS_SECRET_ACCESS_KEY');

    if (accessKeyId == null || accessKeyId == undefined || accessKeySecret == null || accessKeySecret == undefined) {
        throw new Error('Undefined AWS credentials');
    }
    return { accessKeyId: accessKeyId, accessKeySecret: accessKeySecret, region: 'ca-central-1', url: "https://s3.ca-central-1.amazonaws.com" };
}