import { assert, assertEquals } from 'https://deno.land/std@0.98.0/testing/asserts.ts';
import { config } from 'https://deno.land/x/dotenv@v3.2.0/mod.ts';
import { format } from 'https://deno.land/std@0.106.0/datetime/mod.ts';
import * as artifactory from '../src/artifactory.ts';

Deno.test('Nouvelle version Docker (première de la journée)', () => {
    const versionsImagesExistantes: string[] = ['21.10.30-01', '21.10.26-02'];
    const nouvelleVersion = artifactory.nouvelleVersionImageDocker(versionsImagesExistantes);

    const today = format(new Date(), 'yy.MM.dd');
    const todayVersion = `${today}-01`;
    console.info(nouvelleVersion);
    assertEquals(nouvelleVersion, todayVersion);
});

Deno.test('Nouvelle version Docker (deuxième de la journée)', () => {
    const versionsImagesExistantes = ['21.10.30-1', '21.10.26-2'];
    const today = format(new Date(), 'yy.MM.dd');
    versionsImagesExistantes.push(`${today}-07`);
    const todayVersion = `${today}-08`;
    const incrementedVersion = `${today}-09`;

    versionsImagesExistantes.unshift(todayVersion); // https://stackoverflow.com/questions/8159524/javascript-pushing-element-at-the-beginning-of-an-array
    const nouvelleVersion = artifactory.nouvelleVersionImageDocker(versionsImagesExistantes);

    assertEquals(nouvelleVersion, incrementedVersion);
});

Deno.test('Nouvelle version Jar', () => {
    const versionsJarExistantes: string[] = [
        '0.1.0',
        '1.0.0-ena2-1111-19939fa56',
        '1.1.0',
        '1.12.1',
        '1.2.0',
        '1.3.0',
        '11.11.0',
        '11.11.1',
        '11.2.0',
        '2.1.0',
    ];
    const nouvelleVersion = artifactory.nouvelleVersionSemVer(versionsJarExistantes);

    assertEquals(nouvelleVersion, '11.12.0');
});

Deno.test('Appel à Artifactory pour la liste des images ena-identite (docker)', async () => {
    const { username, password } = credentialsArtifactory();

    const images = await artifactory.obtenirVersionsExistantes(
        'enseignement/ena',
        'ena-identite',
        artifactory.TypeArtifact.Docker,
        username,
        password,
    );
    assert(images.length > 0);
});

Deno.test('Appel à Artifactory pour la liste des versions de ena2-lti (npm)', async () => {
    const { username, password } = credentialsArtifactory();

    const versions = await artifactory.obtenirVersionsExistantes(
        'ena2-lti-client',
        'ena2-lti-client',
        artifactory.TypeArtifact.Npm,
        username,
        password,
    );
    assert(versions.length > 0);
});


Deno.test('Appel à Artifactory pour la liste des versions de ena2-core (jar)', async () => {
    const { username, password } = credentialsArtifactory();

    const versions = await artifactory.obtenirVersionsExistantes(
        'ca/ulaval/ena2/ena2-communs-java',
        'ena2-core',
        artifactory.TypeArtifact.Jar,
        username,
        password,
    );
    assert(versions.length > 0);
});

function credentialsArtifactory() {
    config({ export: true });
    const username = Deno.env.get('ARTIFACTORY_USERNAME');
    const password = Deno.env.get('ARTIFACTORY_PASSWORD');

    if (username == null || username == undefined || password == null || password == undefined) {
        throw new Error('Undefined Artifactory credentials');
    }
    return { username, password };
}
