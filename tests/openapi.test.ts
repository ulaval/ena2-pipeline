import { assert, assertEquals } from 'https://deno.land/std@0.98.0/testing/asserts.ts';
import { format } from 'https://deno.land/std@0.106.0/datetime/mod.ts';
import { Openapi } from '../src/openapi.ts';

Deno.test('obtenirVersionCourante pour json valide', async () => {
    //On doit changer le répertoire de travail pour celui du script
    const scriptDir = new URL('.', import.meta.url).pathname;
    Deno.chdir(scriptDir); // Change le workdir

    const testOpenApi = new Openapi('test', 'openapi');
    const version = await testOpenApi.obtenirVersionCourante();

    assertEquals(version, '14.5.0');
});

Deno.test('versionExiste retourne faux si version inexistante', async () => {
    //On doit changer le répertoire de travail pour celui du script
    const scriptDir = new URL('.', import.meta.url).pathname;
    Deno.chdir(scriptDir); // Change le workdir

    const testOpenApi = new Openapi('test', 'openapi');
    const versionExiste = await testOpenApi.versionExiste();

    assertEquals(versionExiste, false);
});

Deno.test('versionExiste retourne vrai si version existe', async () => {
    //On doit changer le répertoire de travail pour celui du script
    const scriptDir = new URL('.', import.meta.url).pathname;
    Deno.chdir(scriptDir); // Change le workdir

    const testOpenApi = new Openapi('test', 'openapi-existante');
    const versionExiste = await testOpenApi.versionExiste();

    assert(versionExiste);
});

Deno.test({
    name: 'versionner - cree un fichier de la version courante',
    async fn() {
        //On doit changer le répertoire de travail pour celui du script
        const scriptDir = new URL('.', import.meta.url).pathname;
        Deno.chdir(scriptDir); // Change le workdir
        const testOpenApi = new Openapi('test', 'openapi');
        let versionExiste = await testOpenApi.versionExiste();

        assertEquals(versionExiste, false);

        await testOpenApi.versionner();

        versionExiste = await testOpenApi.versionExiste();

        assertEquals(versionExiste, true);

        //Ménage des fichiers générés
        const today = format(new Date(), 'yyyy-MM-dd');
        Deno.removeSync(`./openapi/version/openapi-test-v14.5.0-d${today}.json`);
        Deno.removeSync(`./openapi/version/openapi-test-v14.5.0-d${today}.yaml`);
    },
    // following two options deactivate open resource checking
    sanitizeResources: false,
    sanitizeOps: false,
});
