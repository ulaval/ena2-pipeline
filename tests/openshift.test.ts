import { assertEquals } from 'https://deno.land/std@0.98.0/testing/asserts.ts';
import * as oc from '../src/openshift.ts';

Deno.test('transformer map params', () => {
    const mapLabels = new Map<string, string>([
        ['label1', 'value1'],
        ['label2', 'value2'],
    ]);
    const paramLLabels = oc.construireListeParams(mapLabels);
    assertEquals(paramLLabels, 'label1=value1 label2=value2');
});
Deno.test('transformer map labels', () => {
    const mapLabels = new Map<string, string>([
        ['label1', 'value1'],
        ['label2', 'value2'],
    ]);
    const paramLLabels = oc.construireListeLabels(mapLabels);
    assertEquals(paramLLabels, 'label1=value1,label2=value2');
});
