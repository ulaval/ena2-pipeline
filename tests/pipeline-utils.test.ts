import { assertEquals, assertThrows } from 'https://deno.land/std@0.98.0/testing/asserts.ts';
import * as utils from '../src/pipeline-utils.ts';

Deno.test('getEnvName', () => {
    assertEquals(utils.getEnvName('develop'), 'develop');
    assertEquals(utils.getEnvName('master'), 'master');
    assertEquals(utils.getEnvName('main'), 'master');
    assertEquals(utils.getEnvName('feature/ena2-123_galerie-video'), 'ena2-123');
    assertEquals(utils.getEnvName('hotfix/ena2-123_galerie-video'), 'ena2-123');
    assertThrows((): void => { utils.getEnvName('feature/ena2-123-galerie-video'); });
    assertThrows((): void => { utils.getEnvName('feature/galerie-video'); });
});

Deno.test('doRunWithStdout', async () => {
    const value = await utils.doRunWithStdout({ cmd: ['echo', 'hello'] });
    assertEquals(value, 'hello');
});

Deno.test('doExecuteWithStdout', async () => {
    const value = await utils.doExecuteWithStdout('echo', { args: ['hello']});
    assertEquals(value, 'hello');
});
