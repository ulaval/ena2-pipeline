import { assertEquals } from 'https://deno.land/std@0.207.0/assert/mod.ts';
import * as services from '../../src/environnement/services.ts';
import { ServiceDetails } from "../../src/environnement/services.ts";

Deno.test('getDetailsServiceCourant', () => {
    assertEquals(getNomsServices(services.getDetailsServiceCourant('identites', 'DEPLOIEMENT')), ['identites']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('identites', 'DEPLOIEMENT_COMPLET')), ['identites', 'identites-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('audit', 'DEPLOIEMENT')), ['audit']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('audit', 'DEPLOIEMENT_COMPLET')), ['audit', 'audit-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('communications', 'DEPLOIEMENT')), ['communications']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('communications', 'DEPLOIEMENT_COMPLET')), ['communications', 'communications-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('fichiers', 'DEPLOIEMENT')), ['fichiers']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('fichiers', 'DEPLOIEMENT_COMPLET')), ['fichiers', 'fichiers-taches', 'fichiers-conversions-audiovideo', 'fichiers-docgen']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('formations', 'DEPLOIEMENT')), ['formations']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('formations', 'DEPLOIEMENT_COMPLET')), ['formations', 'formations-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('paiements', 'DEPLOIEMENT')), ['paiements']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('paiements', 'DEPLOIEMENT_COMPLET')), ['paiements', 'paiements-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('questionnaires', 'DEPLOIEMENT')), ['questionnaires']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('questionnaires', 'DEPLOIEMENT_COMPLET')), ['questionnaires', 'questionnaires-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('sitepromo', 'DEPLOIEMENT')), ['sitepromo']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('sitepromo', 'DEPLOIEMENT_COMPLET')), ['sitepromo', 'sitepromo-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('sites', 'DEPLOIEMENT')), ['sites']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('sites', 'DEPLOIEMENT_COMPLET')), ['sites', 'sites-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('recherches', 'DEPLOIEMENT')), ['recherches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('recherches', 'DEPLOIEMENT_COMPLET')), ['recherches', 'recherches-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('coordonnateur', 'DEPLOIEMENT')), ['coordonnateur']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('coordonnateur', 'DEPLOIEMENT_COMPLET')), ['coordonnateur', 'coordonnateur-taches']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('lti', 'DEPLOIEMENT')), ['lti']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('lti', 'DEPLOIEMENT_COMPLET')), ['lti']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('synchro-etudes', 'DEPLOIEMENT')), ['synchro-etudes']);
    assertEquals(getNomsServices(services.getDetailsServiceCourant('synchro-etudes', 'DEPLOIEMENT_COMPLET')), ['synchro-etudes']);
});

function getNomsServices (detailsService: ServiceDetails[]): string[] {
    return detailsService.map(service => service.nomService);
}