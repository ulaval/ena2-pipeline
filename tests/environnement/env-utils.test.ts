// deno-lint-ignore-file require-await
import { assertEquals, assertNotMatch, assertMatch } from "https://deno.land/std@0.207.0/assert/mod.ts";
import { determinerOptionsDeploiements } from "../../src/environnement/env-utils.ts";
import { ConfigOpenshift } from "../../src/openshift.ts";

const CONFIG_OC: ConfigOpenshift = { server: "server", namespace: "namespace", token: "token" };

Deno.test('optionsDeploiement => develop & complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('develop-audit', CONFIG_OC, true, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT_COMPLET\nRIEN');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT_COMPLET:)[\\s\\S]*(RIEN:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEPLOIEMENT:)[\\s\\S]*$', 'gm'));
    assertEquals(contenuJson["isServiceExiste"], true);
    assertEquals(contenuJson["isBrancheFeature"], false);
});

Deno.test('optionsDeploiement => develop & !complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('develop-audit', CONFIG_OC, false, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT\nRIEN');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT:)[\\s\\S]*(RIEN:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*$', 'gm'));
});

Deno.test('optionsDeploiement => main & complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('main-audit', CONFIG_OC, true, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT\nRIEN\nDEPLOIEMENT_COMPLET\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT:)[\\s\\S]*(RIEN:)[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertEquals(contenuJson["isServiceExiste"], true);
    assertEquals(contenuJson["isBrancheFeature"], false);
});

Deno.test('optionsDeploiement => main & !complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('main-audit', CONFIG_OC, false, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT\nRIEN\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT:)[\\s\\S]*(RIEN:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*$', 'gm'));
});

Deno.test('optionsDeploiement => master & complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('master-audit', CONFIG_OC, true, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT\nRIEN\nDEPLOIEMENT_COMPLET\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT:)[\\s\\S]*(RIEN:)[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertEquals(contenuJson["isServiceExiste"], true);
    assertEquals(contenuJson["isBrancheFeature"], false);
});

Deno.test('optionsDeploiement => master & !complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('master-audit', CONFIG_OC, false, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT\nRIEN\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT:)[\\s\\S]*(RIEN:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*$', 'gm'));
});

Deno.test('optionsDeploiement => ena2-4385 & complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('ena2-4385-audit', CONFIG_OC, true, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT\nRIEN\nDEPLOIEMENT_COMPLET\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT:)[\\s\\S]*(RIEN:)[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertEquals(contenuJson["isServiceExiste"], true);
    assertEquals(contenuJson["isBrancheFeature"], false);
});

Deno.test('optionsDeploiement => ena2-4385 & !complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('ena2-4385-audit', CONFIG_OC, false, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'DEPLOIEMENT\nRIEN\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(DEPLOIEMENT:)[\\s\\S]*(RIEN:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*$', 'gm'));
});

Deno.test('optionsDeploiement => feature pas déployée & complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('ena2-12345-audit', CONFIG_OC, true, async() => false);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'RIEN\nDEPLOIEMENT\nDEPLOIEMENT_COMPLET');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(RIEN:)[\\s\\S]*(DEPLOIEMENT:)[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertEquals(contenuJson["isServiceExiste"], false);
    assertEquals(contenuJson["isBrancheFeature"], true);
});

Deno.test('optionsDeploiement => feature pas déployée & !complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('ena2-12345-audit', CONFIG_OC, false, async() => false);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'RIEN\nDEPLOIEMENT');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(RIEN:)[\\s\\S]*(DEPLOIEMENT:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
});

Deno.test('optionsDeploiement => feature déployée & complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('ena2-12345-audit', CONFIG_OC, true, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'RIEN\nDEPLOIEMENT\nDEPLOIEMENT_COMPLET\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(RIEN:)[\\s\\S]*(DEPLOIEMENT:)[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertEquals(contenuJson["isServiceExiste"], true);
    assertEquals(contenuJson["isBrancheFeature"], true);
});

Deno.test('optionsDeploiement => feature déployée & !complet', async () => {
    const resultat: string = await determinerOptionsDeploiements('ena2-12345-audit', CONFIG_OC, false, async() => true);

    const contenuJson = JSON.parse(resultat);
    assertEquals(contenuJson["optionsDeploiement"].choix, 'RIEN\nDEPLOIEMENT\nDEMARRER_PODS\nFERMER_PODS');
    assertMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^(RIEN:)[\\s\\S]*(DEPLOIEMENT:)[\\s\\S]*(DEMARRER_PODS:)[\\s\\S]*(FERMER_PODS:)[\\s\\S]*$', 'gm'));
    assertNotMatch(contenuJson["optionsDeploiement"].descriptions, new RegExp('^[\\s\\S]*(DEPLOIEMENT_COMPLET:)[\\s\\S]*$', 'gm'));
});