/**
 * Wrapper de Deno.run qui lance une erreur lorsque le process ne se termine pas avec succès 
 * @deprecated Remplacé par doExecute(cmd, CommandOptions) suite à la déprécation de Deno.run
 */
export async function doRun(opt: Deno.RunOptions) {
    const p = Deno.run({
        cmd: opt.cmd,
        cwd: opt.cwd,
        env: opt.env,
    });

    const status = await p.status();
    p.close();
    if (!status.success) {
        throw new Error('Le process s\'est terminé en erreur. Code: ' + status.code);
    }
}

/** Exécution d'un process qui lance une erreur lorsque le process ne se termine pas avec succès. */
export async function doExecute(cmd: string | URL, opt?: Deno.CommandOptions) {
    const command = new Deno.Command(cmd, {
        args: opt?.args,
        cwd: opt?.cwd,
        env: opt?.env,
        stdout: 'inherit',
    });

    const { success, code, stderr } = await command.output();;
    if (!success) {
        const decodedError = new TextDecoder().decode(stderr);
        throw new Error('Le process s\'est terminé en erreur. Code: ' + code, { cause: {
            error: decodedError
        }});
    }
}

/** Exécution d'un process qui lance une erreur lorsque le process ne se termine pas avec succès. */
export function doExecuteSync(cmd: string | URL, opt?: Deno.CommandOptions) {
    const command = new Deno.Command(cmd, {
        args: opt?.args,
        cwd: opt?.cwd,
        env: opt?.env,
        stdout: 'inherit',
    });

    const { success, code, stderr } = command.outputSync();;
    if (!success) {
        const decodedError = new TextDecoder().decode(stderr);
        throw new Error('Le process s\'est terminé en erreur. Code: ' + code, { cause: {
            error: decodedError
        }});
    }
}

/**
 * Wrapper de Deno.run qui lance une erreur lorsque le process ne se termine pas avec succès
 * @deprecated Remplacé par doExecuteWithStdout(cmd, CommandOptions) suite à la déprécation de Deno.run
 */
export async function doRunWithStdout(opt: Deno.RunOptions): Promise<string> {
    const p = Deno.run({
        cmd: opt.cmd,
        cwd: opt.cwd,
        env: opt.env,
        stdout: 'piped',
    });

    const rawOutput = await p.output();
    const status = await p.status();

    if (!status.success) {
        throw new Error('Le process s\'est terminé en erreur. Code: ' + status.code);
    }

    p.close();
    let decodedOutput = new TextDecoder().decode(rawOutput);
    //Étrangement, il ya toujours un \n à la fin de la chaine de caractère
    decodedOutput = decodedOutput.slice(0, -1);
    return decodedOutput;
}

/** Exécution d'un process avec valeur de retour, qui lance une erreur lorsque le process ne se termine pas avec succès. */
export async function doExecuteWithStdout(cmd: string | URL, opt?: Deno.CommandOptions): Promise<string> {
    const command = new Deno.Command(cmd, {
        args: opt?.args,
        cwd: opt?.cwd,
        env: opt?.env,
        stdout: 'piped',
    });

    const { success, code, stdout, stderr } = await command.output();
    const decodedOutput = new TextDecoder().decode(stdout);
    if (!success) {
        const decodedError = new TextDecoder().decode(stderr);
        throw new Error('Le process s\'est terminé en erreur. Code: ' + code, { cause: {
            error: decodedError,
            output: decodedOutput
        }});
    }
    //Étrangement, il ya toujours un \n à la fin de la chaine de caractère
    return decodedOutput.slice(0, -1);
}

/** Génère le nom de l'environnement à partir de la branche git*/
export function getEnvName(gitBranchName: string): string {
    gitBranchName = gitBranchName.toLowerCase();
    if (['master', 'develop'].some((x) => x == gitBranchName)) {
        return gitBranchName;
    }

    //Le temps d'armoniser tous les envs
    if ('main' == gitBranchName) {
        return 'master';
    }
    const regexp = new RegExp(
        '(feature|hotfix|demo|misc|bugfix)\/(?<envID>[0-9]*\.[0-9]*\.[0-9]*|[a-zA-Z0-9]+-[0-9]+)[_]{1}.*',
    );

    const result = regexp.exec(gitBranchName);

    if (result) {
        const groups = result['groups'];
        if (groups) {
            return groups['envID'];
        }
    }
    throw new Error('Nom de branche invalide:' + gitBranchName);
}

/**
 * Lit un fichier JSON puis le parse en un objet
 * @param filePath
 */
export async function getJson(filePath: string) {
    return JSON.parse(await Deno.readTextFile(filePath));
}

export function publicationNecessaire(filesPath: string[], excludePatterns: string[]): boolean {
    return true;
}

/** Génère le descriptif de l'environnement à partir de la branche git*/
export function getDescriptifEnv(gitBranchName: string): string {
    gitBranchName = gitBranchName.toLowerCase();
    if (['main', 'master', 'develop'].some((x) => x == gitBranchName)) {
        return gitBranchName;
    }
    const regexp = new RegExp(
        '(feature|hotfix|release|demo|misc|bugfix)/(?<idEnv>[a-zA-Z0-9-]+)[_]?(?<descriptif>.*)',
    );

    const result = regexp.exec(gitBranchName);

    if (result) {
        const groups = result['groups'];
        if (groups) {
            return groups['descriptif'];
        }
    }
    throw new Error('Nom de branche invalide:' + gitBranchName);
}

/**
 * Introduit un délai du nombre de millisecondes spécifié
 * @param ms
 */
export async function sleep(ms: number) {
    await new Promise((r) => setTimeout(r, ms));
}
