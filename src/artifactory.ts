import { format } from 'https://deno.land/std@0.106.0/datetime/mod.ts';

export enum TypeArtifact {
    Docker,
    Jar,
    Npm,
}

export interface DockerImage {
    projet: string;
    module: string;
    version?: string;
}

export function nouvelleVersionImageDocker(images: string[]) {
    const versionRegex = new RegExp(/([0-9]{2}\.[0-9]{2}\.[0-9]{2})-([0-9]+)/g);
    const todayVersion = format(new Date(), 'yy.MM.dd');

    if (images.length == 0) {
        return `${todayVersion}-01`;
    }

    const latestImage = images.sort().pop();
    if (latestImage == null || latestImage == undefined) {
        throw new Error('Latest image is undefined');
    }

    const regexMatch = [...latestImage.matchAll(versionRegex)];
    if (regexMatch.length > 0) {
        const dateDerniereVersion = regexMatch[0][1];
        const incrementDerniereVersion: number = +regexMatch[0][2]; // https://stackoverflow.com/questions/14667713/how-to-convert-a-string-to-number-in-typescript
        const increment = incrementDerniereVersion + 1;
        if (dateDerniereVersion == todayVersion) {
            return `${dateDerniereVersion}-${(increment).toString().padStart(2, "0")}`;
        }
    }

    return `${todayVersion}-01`;
}

export function nouvelleVersionSemVer(versions: string[]) {
    const versionsConformes = versions.filter((v) => v.match('^[0-9]+\.[0-9]+\.[0-9]+$'));
    if (versionsConformes.length == 0) {
        return `1.0.0`;
    }

    // Convertir les valeurs en integer, trier selon la version majeure et mineure en ordre croissant
    // le dernier élément représente la version la plus récente, à partir de laquelle on incrémente.
    const versionAIncrementer = versionsConformes
        .map((v) => v.split('.'))
        .map((obj) => [parseInt(obj[0]), parseInt(obj[1]), parseInt(obj[2])])
        .sort((a, b) => {
            if (a[0] === b[0]) {
                return a[1] - b[1];
            }
            return a[0] - b[0];
        }).pop();

    if (versionAIncrementer == null || versionAIncrementer == undefined) {
        throw new Error('Impossible de déterminer la prochaine version');
    }

    return `${versionAIncrementer[0]}.${versionAIncrementer[1] + 1}.0`;
}

export async function getMatchingImagesAndVersions(
    projet: string,
    nomImage: string,
    username: string,
    password: string,
) {
    // Valider les paramètres
    if (!username || !password) {
        throw new Error('Les paramètres username et password sont obligatoires');
    }
    const results = await searchArtifactory(
        'docker-local',
        `enseignement/${projet}/${nomImage}*`,
        'manifest.json',
        username,
        password,
    );
    const versionRegex = new RegExp(/([0-9]{2}\.[0-9]{2}\.[0-9]{2}-[0-9]+$)/);
    const matchingImages = [];
    for (const item of results) {
        const pathElements = item['path'].split('/');
        const tag = pathElements.pop();
        const name = pathElements.pop();

        if (name.startsWith(nomImage) && versionRegex.test(tag)) {
            matchingImages.push({ name: name, version: tag });
        }
    }

    return matchingImages.sort((a, b) => {
        if (a.version > b.version) {
            return 1;
        }

        if (a.version < b.version) {
            return -1;
        }

        return 0;
    });
}

export async function obtenirVersionsExistantes(
    path: string,
    module: string,
    type: TypeArtifact,
    username: string,
    password: string,
) {
    // Valider les paramètres
    if (!username || !password) {
        throw new Error('Les paramètres username et password sont obligatoires');
    }

    let nomRepo = '';
    let nomArtifact = '';
    let versionRegex = null;
    let pathArtifact = '';

    switch (type) {
        case TypeArtifact.Docker:
            nomRepo = 'docker-local';
            nomArtifact = 'manifest.json';
            pathArtifact = `${path}/${module}`;
            versionRegex = new RegExp(/([0-9]{2}\.[0-9]{2}\.[0-9]{2}-[0-9]+)/);
            break;
        case TypeArtifact.Jar:
            nomRepo = 'libs-releases-local';
            nomArtifact = `${module}-*.jar`;
            pathArtifact = `${path}/${module}`;
            versionRegex = new RegExp(/([0-9]+\.[0-9]+\.[0-9]+$)/);
            break;
        case TypeArtifact.Npm:
            nomRepo = 'npm-local';
            nomArtifact = `${module}-*.tgz`;
            pathArtifact = `*/${path}`;
            versionRegex = new RegExp(/([0-9]+\.[0-9]+\.[0-9]+$)/);
            break;
        default:
            throw new Error('Type d\'artifacte invalide');
    }

    const results = await searchArtifactory(nomRepo, pathArtifact, nomArtifact, username, password);

    const versions = [];
    for (const item of results) {
        let tag = '';
        if (type === TypeArtifact.Npm) {
            const pathElements = item['name'].split(`${module}-`);
            const tagAvecExt = pathElements.pop();
            const tagElements = tagAvecExt.split(`.tgz`);
            tag = tagElements.shift();
        } else {
            const pathElements = item['path'].split('/');
            tag = pathElements.pop();
        }

        if (versionRegex.test(tag)) {
            versions.push(tag);
        }
    }

    return versions.sort();
}

export async function deleteDockerImage(image_name: string, username: string, password: string) {
    // Valider les paramètres
    if (!username || !password) {
        throw new Error('Les paramètres username et password sont obligatoires');
    }

    const url = `https://maven.ulaval.ca/docker-local/enseignement/${image_name}`;
    console.log(`url: ${url}`);

    const headers = new Headers();
    headers.append('Authorization', `Basic ${btoa(username + ':' + password)}`);
    headers.append('Content-Type', 'application/json');

    const response = await fetch(url, {
        method: 'DELETE',
        headers: headers,
        body: null,
    });

    const status = response.status;
    console.log(`response: ${status}`);
}

async function searchArtifactory(
    repo: string,
    pathArtifact: string,
    nomArtifact: string,
    username: string,
    password: string,
) {
    // Valider les paramètres
    if (!username || !password) {
        throw new Error('Les paramètres username et password sont obligatoires');
    }

    // Valider les paramètres
    if (!username || !password) {
        throw new Error(
            'Les paramètres ARTIFACTORY_USERNAME et ARTIFACTORY_PASSWORD sont obligatoires',
        );
    }

    // fetch ref: https://developer.mozilla.org/en-US/docs/Web/API/fetch
    // Headers ref: https://developer.mozilla.org/en-US/docs/Web/API/Headers
    // Basic auth ref: https://stackoverflow.com/questions/43842793/basic-authentication-with-fetch
    // Base64 ref: https://medium.com/deno-the-complete-reference/base64-encoding-decoding-in-deno-bfaafc5471db
    const headers = new Headers();
    headers.append('Content-Type', 'text/plain');
    headers.append('Authorization', `Basic ${btoa(username + ':' + password)}`);

    const body = `
        items
            .find({
                "repo": {
                    "$eq":"${repo}"
                },
                "$and": [
                    {
                        "path": {
                            "$match": "${pathArtifact}/*"
                        }
                    },
                    {
                        "name": {
                            "$match": "${nomArtifact}"
                        }
                    }
                ]
            })
            .include( "path", "repo", "name")
            .sort({
                "$asc": ["path"]
            })
    `.replace(/\s/g, '');

    const options = {
        method: 'POST',
        headers: headers,
        body: body,
    };

    const response = await fetch('https://maven.ulaval.ca/api/search/aql', options);
    if (response.status != 200) {
        console.log(`Return response: ${response.status} -- ${response.body}`);
    }

    const content = await response.json();
    return content['results'];
}
