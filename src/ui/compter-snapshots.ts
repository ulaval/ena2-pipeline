/**
 * Script qui ajoute la date, le nom de la branche, ainsi qu'un nombre à la fin
 * du fichier snapshots.csv du bucket ena2-docs sur https://s3.dev.brioeducation.ca/
 * Disponible en lecture sur https://s3.dev.brioeducation.ca/ena2-docs/snapshots.csv
 *
 * Ex. deno run --allow-run --allow-read --allow-write compterSnapshots.ts --accessKeyIdS3=?? --accessKeySecretS3=?? --nbreSnapshots=28
 *
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import { format } from 'https://deno.land/std@0.106.0/datetime/mod.ts';
import * as utils from "https://bitbucket.org/ulaval/ena2-pipeline/raw/22.11.07-1/src/pipeline-utils.ts";
import { AwsConfig } from "https://bitbucket.org/ulaval/ena2-pipeline/raw/22.11.07-1/src/aws/awsconfig.ts";
import * as s3 from "https://bitbucket.org/ulaval/ena2-pipeline/raw/23.08.03-2/src/aws/s3.ts";

const { endpointUrlS3, accessKeyIdS3, accessKeySecretS3, branchName, nbreSnapshots } = parse(Deno.args, {
    default: {
        endpointUrlS3: "https://s3.dev.brioeducation.ca",
        nbreSnapshots: 0
    }
});

//Valider les paramètre
if (!accessKeyIdS3 || !accessKeySecretS3) {
    throw new Error("Les credentials S3 sont obligatoires");
}


const configAWS: AwsConfig = {
    accessKeyId: accessKeyIdS3,
    accessKeySecret: accessKeySecretS3,
    region: 'ca-central-1',
    url: endpointUrlS3
};


const nowDate = format(new Date(), 'yyyy-MM-dd HH:mm:ss');
await s3.copyLocally(configAWS, 'ena2-docs/snapshots.csv', './snapshots.csv');
await Deno.writeTextFile('./snapshots.csv', `${nowDate},${branchName},${nbreSnapshots}\n`, { append: true, create: false });
await s3.copyToBucket(configAWS, 'ena2-docs', './snapshots.csv');
await Deno.remove("./snapshots.csv");



