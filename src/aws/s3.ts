import { doExecute, getEnvName } from "../pipeline-utils.ts";
import { AwsConfig } from "./awsconfig.ts";


/**
 * Copie un fichier local dans un bucket S3
 *
 * @param config La  config AWS
 * @param s3uri L'uri s3 du fichier
 * @param cheminFichier Chemin du fichier local
 */
export async function copyToBucket(
    config: AwsConfig,
    s3uri: string,
    cheminFichier: string
) {
    await doExecute('bash', {
        args: [
            '-c',
            `AWS_ACCESS_KEY_ID=${config.accessKeyId} ` +
            `AWS_SECRET_ACCESS_KEY=${config.accessKeySecret} ` +
            `aws s3 cp ${cheminFichier} s3://${s3uri}/ ` +
            `--endpoint-url=${config.url} --region=${config.region}`
        ],
    });
}

/**
 * Copie récursivement le contenu d'un répertoire local dans un bucket S3
 *
 * @param config La  config AWS
 * @param s3uri L'uri s3 du fichier
 * @param cheminFichier Chemin du fichier local
 */
export async function copyRecursivelyToBucket(
    config: AwsConfig,
    s3uri: string,
    cheminFichier: string
) {
    await doExecute('bash', {
        args: [
            '-c',
            `AWS_ACCESS_KEY_ID=${config.accessKeyId} ` +
            `AWS_SECRET_ACCESS_KEY=${config.accessKeySecret} ` +
            `aws s3 cp ${cheminFichier} s3://${s3uri}/ ` +
            `--endpoint-url=${config.url} --region=${config.region} --recursive`
        ],
    });
}

/**
 * Copie un fichier d'un bucket s3 vers un fichier local
 *
 * @param config La  config AWS
 * @param s3uri L'uri s3 du fichier
 * @param cheminFichier Chemin du fichier local
 */
export async function copyLocally(
    config: AwsConfig,
    s3uri: string,
    cheminFichier: string
) {
    await doExecute('bash', {
        args: [
            '-c',
            `AWS_ACCESS_KEY_ID=${config.accessKeyId} ` +
            `AWS_SECRET_ACCESS_KEY=${config.accessKeySecret} ` +
            `aws s3 cp s3://${s3uri} ${cheminFichier} ` +
            `--endpoint-url=${config.url} --region=${config.region}`
        ],
    });
}

/**
 * Crée un bucket S3 public en lecture
 *
 * @param config La  config AWS
 * @param nomBucket Nom du bucket
 */
export async function creerBucket(config: AwsConfig, nomBucket: string): Promise<boolean> {

    console.log("Debut Création du bucket S3");
    if (!await isBucketExiste(config, nomBucket)) {
        console.log("Création du bucket S3");
        try {
            await doExecute('/bin/sh', {
                args: [
                    '-c',
                    `AWS_ACCESS_KEY_ID=${config.accessKeyId} ` +
                    `AWS_SECRET_ACCESS_KEY=${config.accessKeySecret} ` +
                    `aws --region=${config.region} s3api create-bucket --bucket=${nomBucket} ` +
                    `--endpoint-url=${config.url} --cli-read-timeout 60`
                ],
            });
        } catch (err) {
            if (!await isBucketExiste(config, nomBucket)) {
                throw err;
            }
        }

        const defpolicy = '\"{\\"Version\\":\\"2012-10-17\\",\\"Statement\\":[{\\"Effect\\":\\"Allow\\",\\"Principal\\":{\\"AWS\\":[\\"*\\"]},\\"Action\\":[\\"s3:GetBucketLocation\\",\\"s3:ListBucket\\"],\\"Resource\\":[\\"arn:aws:s3:::' +
            `${nomBucket}` +
            '\\"]},{\\"Effect\\":\\"Allow\\",\\"Principal\\":{\\"AWS\\":[\\"*\\"]},\\"Action\\":[\\"s3:GetObject\\"],\\"Resource\\":[\\"arn:aws:s3:::' +
            `${nomBucket}/` +
            '*\\"]}]}\"'

        console.log("defpolicy" + defpolicy);
        await putBucketPolicy(config, nomBucket, defpolicy);
        return true;
    }
    return false;
}

export async function isBucketExiste(config: AwsConfig, nomBucket: string): Promise<boolean> {
    try {
        await doExecute('/bin/sh', {
            args: [
                '-c',
                `AWS_ACCESS_KEY_ID=${config.accessKeyId} ` +
                `AWS_SECRET_ACCESS_KEY=${config.accessKeySecret} ` +
                `aws --region=${config.region} s3api head-bucket --bucket=${nomBucket} ` +
                `--endpoint-url=${config.url} --cli-read-timeout 60`
            ],
        });
        return true;
    } catch (_error) {
        return false;
    }
}

async function putBucketPolicy(config: AwsConfig, nomBucket: string, policy: string) {
    await doExecute('/bin/sh', {
        args: [
            '-c',
            `AWS_ACCESS_KEY_ID=${config.accessKeyId} ` +
            `AWS_SECRET_ACCESS_KEY=${config.accessKeySecret} ` +
            `aws --region=${config.region} s3api put-bucket-policy --bucket=${nomBucket} --policy ${policy} ` +
            `--endpoint-url=${config.url} --cli-read-timeout 60`
        ],
    });

}


export async function initS3(
    configAWS: AwsConfig,
    branchName: string,
    rechargerDonnees: boolean,
) {
    const nomBucketS3 = genererNomBucketS3(branchName);
    const bucketExiste = await isBucketExiste(configAWS, nomBucketS3);
    console.log(`Bucket s3 ${nomBucketS3} existe? : ${bucketExiste}`);

    //Créer bucket s3
    if (!bucketExiste) {
        // Création du bucket

        console.log("Création du bucket : " + nomBucketS3);

        const creation = await creerBucket(configAWS, nomBucketS3);
        console.log("Création : " + creation);
    }
    //Si rechargerDonnees ou nouveau bucket, on charge les données
    if (rechargerDonnees || !bucketExiste) {
        console.log("Debut sync des donnees...");
        await doExecute("docker", {
            args: [
                "run",
                "--pull=always",
                "-t",
                "docker-local.maven.ulaval.ca/enseignement/ena2/ena2-s3-donnees-dev:latest",
                "bash",
                "-c",
                `AWS_ACCESS_KEY_ID=${configAWS.accessKeyId} ` +
                `AWS_SECRET_ACCESS_KEY=${configAWS.accessKeySecret} ` +
                `aws --region=${configAWS.region} s3 sync /ena2-s3/donnees/dev s3://${nomBucketS3} ` +
                `--endpoint-url=${configAWS.url} --delete`,
            ],
        });
    }
}

function genererNomBucketS3(nomBranche: string): string {
    const nomEnvironnement = getEnvName(nomBranche);
    return `docs-dv-ena2-${nomEnvironnement}-ulaval-ca`;
}

/**
 * Crée un bucket S3 avec l'image docker cdk
 * @param config La  config AWS
 * @param nomBucket Nom du bucket
 */
export async function creerBucketWithCDK(configAWS: AwsConfig, nomBranche: string) {

    console.log("Lancement du cdk pour la création du bucket");
    const nomEnvironnement = getEnvName(nomBranche);
    await doExecute("docker", {
        args: [
            "run",
            "--pull=always",
            "-t",
            "docker-local.maven.ulaval.ca/enseignement/ena2/ena2-cdk-s3-dev:24.01.12-4",
            "bash",
            "-c",
            `AWS_ACCESS_KEY_ID=${configAWS.accessKeyId} ` +
            `AWS_SECRET_ACCESS_KEY=${configAWS.accessKeySecret} ` +
            `cdk deploy --context stackName=${nomEnvironnement} --require-approval never `
        ],
    });


}


export async function initS3WithCDK(
    configAWS: AwsConfig,
    branchName: string,
    rechargerDonnees: boolean,
) {
    const nomBucketS3 = genererNomBucketS3(branchName);
    //const bucketExiste = await isBucketExiste(configAWS, nomBucketS3);
    // console.log(`Bucket s3 ${nomBucketS3} existe? : ${bucketExiste}`);

    //Créer bucket s3

    // Création du bucket

    console.log("Création du bucket : " + nomBucketS3);

    const creation = await creerBucketWithCDK(configAWS, branchName);
    console.log("Création : " + creation);

    //Si rechargerDonnees ou nouveau bucket, on charge les données
    // if (rechargerDonnees || !bucketExiste) {
    console.log("Debut sync des donnees...");
    // await doExecute("docker", {
    //     args: [
    //         "run",
    //         "--pull=always",
    //         "-t",
    //         "docker-local.maven.ulaval.ca/enseignement/ena2/ena2-s3-donnees-dev:latest",
    //         "bash",
    //         "-c",
    //         `AWS_ACCESS_KEY_ID=${configAWS.accessKeyId} ` +
    //         `AWS_SECRET_ACCESS_KEY=${configAWS.accessKeySecret} ` +
    //         `aws --region=${configAWS.region} s3 sync /ena2-s3/donnees/dev s3://${nomBucketS3} ` +
    //         `--endpoint-url=${configAWS.url} --delete`,
    //     ],
    // });
    //  }
}




