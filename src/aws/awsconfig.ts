/**
 * Objet des paramètres de configuration pour les commandes AWS 
 */
export interface AwsConfig {
  accessKeyId: string;
  accessKeySecret: string;
  region: string;
  url?: string;
}
