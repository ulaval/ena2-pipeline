import * as utils from './pipeline-utils.ts';

/**
 * Objet des paramètres de configuration du Client OC
 */
export interface ConfigOpenshift {
    server: string;
    namespace: string;
    token: string;
}

/**
 * Applique les configurations du fichier en paramètre sur les resources du fichier (oc apply)
 *
 * @param config La  config nécessaire au client OC
 * @param pathTemplate Le path du template
 * @param labelsTemplate Un map des labels à appliquer au gabarit
 * @param paramsTemplate Un map des parametres à appliquer au gabarit
 */
export async function apply(
    config: ConfigOpenshift,
    pathTemplate: string,
    labelsTemplate: Map<string, string>,
    paramsTemplate: Map<string, string> = new Map(),
) {
    let listeLabelsTemplate = construireListeLabels(labelsTemplate);
    let listeParamsTemplate = construireListeParams(paramsTemplate);

    listeLabelsTemplate = listeLabelsTemplate ? `-l ${listeLabelsTemplate}` : ``;
    listeParamsTemplate = listeParamsTemplate ? `-p ${listeParamsTemplate}` : ``;
    await utils.doExecute(
        'bash',
        {args: [
            '-c',
            'oc process ' +
            `--server=${config.server} ` +
            `--token=${config.token} ` +
            `--namespace=${config.namespace} ` +
            `${listeParamsTemplate} ` +
            `${listeLabelsTemplate} ` +
            `-f ${pathTemplate} ` +
            '| oc apply ' +
            `--server=${config.server} ` +
            `--token=${config.token} ` +
            `--namespace=${config.namespace} ` +
            '-f -',
        ]}
    );
}

/**
 * Crée les configurations du fichier en paramètre sur les resources du fichier (oc create)
 *
 * @param config La  config nécessaire au client OC
 * @param pathTemplate Le path du template
 * @param labelsTemplate Un map des labels à appliquer au gabarit
 * @param paramsTemplate Un map des parametres à appliquer au gabarit
 */
export async function create(
    config: ConfigOpenshift,
    pathTemplate: string,
    labelsTemplate: Map<string, string>,
    paramsTemplate: Map<string, string> = new Map(),
) {
    let listeLabelsTemplate = construireListeLabels(labelsTemplate);
    let listeParamsTemplate = construireListeParams(paramsTemplate);

    listeLabelsTemplate = listeLabelsTemplate ? `-l ${listeLabelsTemplate}` : ``;
    listeParamsTemplate = listeParamsTemplate ? `-p ${listeParamsTemplate}` : ``;
    await utils.doExecute(
        'bash',
        {args: [
            '-c',
            'oc process ' +
            `--server=${config.server} ` +
            `--token=${config.token} ` +
            `--namespace=${config.namespace} ` +
            `${listeParamsTemplate} ` +
            `${listeLabelsTemplate} ` +
            `-f ${pathTemplate} ` +
            '| oc create ' +
            `--server=${config.server} ` +
            `--token=${config.token} ` +
            `--namespace=${config.namespace} ` +
            '-f -',
        ]}
    );
}

/**
 * Applique les configurations du fichier en paramètre sur les resources du fichier (oc process), sans faire un apply
 *
 * @param config La  config nécessaire au client OC
 * @param pathTemplate Le path du template
 * @param labelsTemplate Un map des labels à appliquer au gabarit
 * @param paramsTemplate Un map des parametres à appliquer au gabarit
 */
export async function process(
    config: ConfigOpenshift,
    pathTemplate: string,
    outputFile: string,
    paramsTemplate: Map<string, string> = new Map(),
) {

    let listeParamsTemplate = construireListeParams(paramsTemplate);

    listeParamsTemplate = listeParamsTemplate ? `-p ${listeParamsTemplate}` : ``;
    await utils.doExecute(
        'bash',
        {args: [
            '-c',
            'oc process ' +
            `--server=${config.server} ` +
            `--token=${config.token} ` +
            `--namespace=${config.namespace} ` +
            `${listeParamsTemplate} ` +
            `-f ${pathTemplate} ` +
            `>  ${outputFile}`,
        ],
    });
}

/**
 * Une combinaison du rollout latest et rollout status -w. Permet de déployer la dernière version d'un deploymentconfig et de suivre le statut du déploiement.
 * @param config La  config nécessaire au client OC
 * @param nomDeploymentConfig
 */
export async function rolloutLatestAndWatch(config: ConfigOpenshift, nomDeploymentConfig: string) {
    // Lancer rollout
    await utils.doExecute(
        'oc',
        {args: [
            `--server=${config.server}`,
            `--token=${config.token}`,
            `--namespace=${config.namespace}`,
            `rollout`,
            `latest`,
            `dc/${nomDeploymentConfig}`,
        ]},
    );

    try {
        // Affichage du statut et attente de la fin du déploiement
        await utils.doExecute(
            'oc',
            {args: [
                `--server=${config.server}`,
                `--token=${config.token}`,
                `--namespace=${config.namespace}`,
                `rollout`,
                `status`,
                `dc/${nomDeploymentConfig}`,
                '-w',
            ],
        });
    } catch(error) {
        // En cas d'erreur, effectue un scale down du service pour ne pas épuiser les ressources avec des CrashLoop
        const labels = new Map<string, string>();
        labels.set('app', nomDeploymentConfig);
        await scale(config, 0, labels);
        throw error;
    }
}

export function construireListeLabels(mapLabels: Map<string, string>): string {
    const listeLabels: string[] = [];
    mapLabels.forEach((value, key) => {
        listeLabels.push(`${key}=${value}`);
    });

    return listeLabels.join(',');
}

export function construireListeParams(mapLabels: Map<string, string>): string {
    const listeLabels: string[] = [];
    mapLabels.forEach((value, key) => {
        listeLabels.push(`${key}=${value}`);
    });

    return listeLabels.join(' ');
}

export async function getQuota(config: ConfigOpenshift, quotaName: string) {
    const quota = await utils.doRunWithStdout({
        cmd: [
            'oc',
            `--server=${config.server}`,
            `--token=${config.token}`,
            `--namespace=${config.namespace}`,
            'get',
            'quota',
            `${quotaName}`,
            '-o',
            'json',
        ],
    });

    return JSON.parse(quota);
}

export function parseQuotaForLimits(quota: any) {
    const cpu_limite = quota['status']['hard']['limits.cpu'];
    const memoire_limite = quota['status']['hard']['limits.memory'];
    const cpu_utilise = quota['status']['used']['limits.cpu'];
    const memoire_utilise = quota['status']['used']['limits.memory'];

    return {
        cpu_utilise: parseCPU(cpu_utilise),
        cpu_limite: parseCPU(cpu_limite),
        memoire_utilise: parseMemory(memoire_utilise),
        memoire_limite: parseMemory(memoire_limite),
    };
}

export async function scale(
    config: ConfigOpenshift,
    nombreReplicas: number,
    labels: Map<string, string>,
) {
    const listeLabels = construireListeLabels(labels);

    return await utils.doRunWithStdout({
        cmd: [
            'oc',
            `--server=${config.server}`,
            `--token=${config.token}`,
            `--namespace=${config.namespace}`,
            `--replicas=${nombreReplicas}`,
            `-l ${listeLabels}`,
            `scale`,
            `dc`,
        ],
    });
}

export async function getNbPods(
    config: ConfigOpenshift,
    labels: Map<string, string>,
    fieldQuery: string,
): Promise<number> {
    const listeLabels = construireListeLabels(labels);
    const resultat = await utils.doRunWithStdout({
        cmd: [
            'oc',
            `--server=${config.server}`,
            `--token=${config.token}`,
            `--namespace=${config.namespace}`,
            'get',
            'pods',
            `-l ${listeLabels}`,
            `--field-selector=${fieldQuery}`,
            '-o',
            'json',
        ],
    });

    return JSON.parse(resultat).items.length;
}


export async function isAppExiste(
    config: ConfigOpenshift,
    nomApp: string
): Promise<boolean> {
    let result = "";
    try {
        result = await utils.doExecuteWithStdout('oc',{
            args: [
                `--server=${config.server}`,
                `--token=${config.token}`,
                `--namespace=${config.namespace}`,
                'get',
                `dc/${nomApp}`,
                '--ignore-not-found'
            ],
        });
    } catch (err) {
        console.log("Error:" + err.message);
        return false;
    }
    return result.includes(nomApp);
}

export async function isPodExiste(
    config: ConfigOpenshift,
    nomPod: string
): Promise<boolean> {
    let result = "";
    try {
        result = await utils.doRunWithStdout({
            cmd: [
                'oc',
                `--server=${config.server}`,
                `--token=${config.token}`,
                `--namespace=${config.namespace}`,
                'get',
                `pod`,
                `-l`,
                `name=${nomPod}`
            ],
        });

    } catch (err) {
        console.log("Error:" + err.message);
        return false;
    }
    return result.includes(nomPod);
}

export async function isPodTagForwardLogsCloud(
    config: ConfigOpenshift,
    nomPod: string,
    logsNamespace: string
): Promise<boolean> {
    let result = "";
    try {
        result = await utils.doRunWithStdout({
            cmd: [
                'oc',
                `--server=${config.server}`,
                `--token=${config.token}`,
                `--namespace=${config.namespace}`,
                'get',
                `pod`,
                `-l`,
                `name=${nomPod},forwardLogsCloud=${logsNamespace}`
            ],
        });

    } catch (err) {
        console.log("Error:" + err.message);
        return false;
    }
    return result.includes(nomPod);
}

export async function labelPods(
    config: ConfigOpenshift,
    labelsPods: Map<string, string>,
    nomLabel: string,
    valeurLabel: string
) {
    const listeLabels = construireListeLabels(labelsPods);
    console.log(`oc label pods --overwrite=true --server=${config.server} --namespace=${config.namespace} -l ${listeLabels} ${nomLabel}=${valeurLabel}`);
    await utils.doExecute(
        'oc',
        {args: [
            'label',
            'pods',
            `--overwrite=true`,
            `--server=${config.server}`,
            `--token=${config.token}`,
            `--namespace=${config.namespace}`,
            `-l ${listeLabels}`,
            `${nomLabel}=${valeurLabel}`,

        ],
    });
}

/**
 * Permet de supprimer un label d'un ou plusieurs pods sur Openshift.
 * 
 * @param config La config Openshift.
 * @param tous Indique si l'on veut modifier tous les pods du namespace.
 * @param labelsPods La liste des labels permettant de filtrer les pods à modifier. Ignorée si la valeur du paramètre 'tous' est true.
 * @param nomLabel Le nom du label à supprimer.
 */
export async function supprimerLabelPods(
    config: ConfigOpenshift,
    tous: boolean,
    labelsPods: Map<string, string>,
    nomLabel: string
) {
    const filtrePods: string = tous ? '--all' : '-l ' + construireListeLabels(labelsPods);
    console.log(`oc label pods --server=${config.server} --namespace=${config.namespace} ${filtrePods} ${nomLabel}-`);
    await utils.doExecute(
        'oc',
        {args: [
            'label',
            'pods',
            `--server=${config.server}`,
            `--token=${config.token}`,
            `--namespace=${config.namespace}`,
            `${filtrePods}`,
            `${nomLabel}-`
        ]
    });
}

function parseMemory(memory_string: string): number {
    const match: any = memory_string.match(/-?\d+\.?\d*/);
    const memory: number = parseInt(match, 10);

    if (memory_string.endsWith('Ki')) {
        return memory * 1024;
    } else if (memory_string.endsWith('Mi')) {
        return memory * 1024 * 1024;
    } else if (memory_string.endsWith('Gi')) {
        return memory * 1024 * 1024 * 1024;
    } else if (memory_string.endsWith('K')) {
        return memory * 1000;
    } else if (memory_string.endsWith('M')) {
        return memory * 1000 * 1000;
    } else if (memory_string.endsWith('G')) {
        return memory * 1000 * 1000 * 1000;
    }

    return memory;
}

function parseCPU(cpu_string: string): number {
    const match: any = cpu_string.match(/-?\d+\.?\d*/);
    const cpu: number = parseInt(match, 10);
    if (cpu_string.endsWith('m')) {
        return cpu / 1000;
    }
    return cpu;
}
