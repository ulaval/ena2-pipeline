/** Script qui permet di'initialiser l'environnement des applcations ENA2 si nécessaire
 *
 * Crée la BD oracle et le bucketS3. Sinon, recharge les données
 * Ex. deno run --allow-read --allow-run --allow-net initEnvironnement.ts --token=$(oc whoami -t) 
 *
 * @deprecated utiliser plutôt les scripts séparés initOracle, initS3 et initService
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as utils from "../pipeline-utils.ts";
import { ConfigOpenshift } from "https://bitbucket.org/ulaval/ena2-pipeline/raw/22.11.07-1/src/openshift.ts";
import { AwsConfig } from "https://bitbucket.org/ulaval/ena2-pipeline/raw/22.11.07-1/src/aws/awsconfig.ts";
import { OracleConfig } from 'https://bitbucket.org/ulaval/ena2-pipeline/raw/22.11.07-1/src/oracle/oracleConfig.ts';
import * as oracle from "../oracle/oracle.ts";
import * as s3 from "../aws/s3.ts";
import * as services from "./services.ts";

const { server, token, namespace, endpointUrlS3, accessKeyIdS3, accessKeySecretS3, rechargerDonnees,
    nomUtilisateurOracle, motDePasseOracle, nomHoteOracle, portOracle, serviceOracle,
    mavenSettingsPath, mavenProfil, serviceCourant } = parse(Deno.args, {
        boolean: ["rechargerDonnees"],
        default: {
            server: "https://api.ul-pca-pr-ul02.ulaval.ca:6443",
            namespace: "ul-ena2-dv01",
            endpointUrlS3: "https://s3.dev.brioeducation.ca",
            rechargerDonnees: false,
            nomHoteOracle: 'exaciidv-scan.x.ul.ca',
            portOracle: '1521',
            serviceOracle: 'e2dv.ul.ca'
        }
    });


//Valider les paramètre
if (!token) {
    throw new Error("Le paramètre token est obligatoire");
}

const configOC: ConfigOpenshift = { server: server, namespace: namespace, token: token };

if (!accessKeyIdS3 || !accessKeySecretS3) {
    throw new Error("Les credentials S3 sont obligatoires");
}

if (!nomUtilisateurOracle || !motDePasseOracle) {
    throw new Error("Les credentials Oracle sont obligatoires");
}

if (!mavenSettingsPath || !mavenProfil) {
    throw new Error("Les config maven sont obligatoires");
}


const currentWorkingDirectory = Deno.cwd();
await utils.doRun({
    cmd: ["git", "config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});

const nomBranche = await utils.doRunWithStdout({
    cmd: ["git", "branch", "--show-current"]
});
const envName = utils.getEnvName(nomBranche);

console.log(`Initialisation pour env ${envName} (branche git ${nomBranche}) `);

const isBrancheFeature = new RegExp('^((?!develop|main|master|ena2-4385).)*$').test(envName);
//Valider le nom de la branche
if (!isBrancheFeature) {
    console.warn("Impossible d'initialiser les environnements permanents");
    Deno.exit(0);
}

const configAWS: AwsConfig = {
    accessKeyId: accessKeyIdS3,
    accessKeySecret: accessKeySecretS3,
    region: 'ca-central-1',
    url: endpointUrlS3
};

const oracleConfig: OracleConfig = {
    nomUtilisateur: nomUtilisateurOracle,
    motDePasse: motDePasseOracle,
    hoteOracle: nomHoteOracle,
    portOracle: portOracle,
    serviceOracle: serviceOracle
};
const nouvelEnvironnement = !(await services.isServiceExiste(configOC, `${envName}-sites`));

console.log(`Forcer un rechargement? ${rechargerDonnees}`);
console.log(`Services ena2 déployés (${envName}-sites)? ${!nouvelEnvironnement}`);

await s3.initS3(configAWS, nomBranche, rechargerDonnees);
await oracle.initBD(oracleConfig, nomBranche, mavenSettingsPath, mavenProfil, rechargerDonnees);

//Déployer les autres services ena2
if (nouvelEnvironnement || rechargerDonnees) {
    services.deployerServices(configOC, [serviceCourant]);
}