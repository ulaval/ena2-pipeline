import * as utils from "../pipeline-utils.ts";

export function getEnvNameSansTiret(envName: string): string {
    return envName.replace('-', '.');
}


function construireListeKeyValue(mapLabels: Map<string, string>): string {
    const listeLabels: string[] = [];
    mapLabels.forEach((value, key) => {
        listeLabels.push(`${key}=${value}`);
    });

    return listeLabels.join(',');
}

function construireListeParamsValue(mapLabels: Map<string, string>): string {
    const listeLabels: string[] = [];
    mapLabels.forEach((value, key) => {
        listeLabels.push(`${key}=${value}`);
    });

    return listeLabels.join(' ');
}

export function construireLabelsModuleADeployer(envName: string, branchName: string, nomModule: string): string {
    let labels = new Map<string, string>();
    labels.set('app', envName)
        .set('description', utils.getDescriptifEnv(branchName));
    let listeLabelsTemplate = construireListeKeyValue(labels);
    return listeLabelsTemplate = listeLabelsTemplate ? `-l ${listeLabelsTemplate}` : ``;;
}

export function construireParamsConfigMap(
    nomModule: string,
    nomApplication: string,
    envName: string,
    envNameSansTiret: string,
    nomSchemaOracle: string): string {
    let params = new Map<string, string>([
        ['NOM_SCHEMA_ORACLE', nomSchemaOracle],
        ['PALIER_NOM_ENV', 'dv.' + envNameSansTiret],
        ['NOM_ENVIRONNEMENT', envName],
        ['NOM_APPLICATION', nomApplication]
    ]);
    switch (nomModule) {
        case "ena2-identites": {
            if (['main', 'develop', 'master'].some(x => x == envName)) {
                params.set('BD_MAX_POOL_SIZE', '20');
            }
            parametresConfigMapMultiUI(envName, params);
            parametresConfigMapEna1(envName, params);
            break;
        }
        case "ena2-formations": {
            if (['main', 'develop', 'master'].some(x => x == envName)) {
                params.set('BD_MAX_POOL_SIZE', '10');
            }
            parametresConfigMapMultiUI(envName, params);
            parametresConfigMapEna1(envName, params);
            break;
        }
        case "ena2-sites": {
            if (['main', 'develop', 'master'].some(x => x == envName)) {
                params.set('BD_MAX_POOL_SIZE', '10');
            }
            break;
        }
    }

    let listeParamsTemplate = construireListeParamsValue(params);
    listeParamsTemplate = listeParamsTemplate ? `-p ${listeParamsTemplate}` : ``;
    return listeParamsTemplate;
}

function parametresConfigMapMultiUI(envName: string, params: Map<string, string>) {
    if (envName == 'main') {
        params.set('MULTI_UI', 'false');
    }
}

function parametresConfigMapEna1(envName: string, params: Map<string, string>) {
    if (envName == 'main') {
        params.set('ENV_ENA1', 'test');
    } else if (envName == 'develop') {
        params.set('ENV_ENA1', 'dev');
    }
}

export function construireParamsDeploymentConfig(nomModule: string, envName: string, profilsSpring: string): string {
    let params = new Map<string, string>([
        ['NOM_ENVIRONNEMENT', envName],
        ['PROFILES_SPRING', `-Dspring.profiles.active=${profilsSpring}`],
        ['NOM_CLE_CHIFFREMENT', 'cle-chiffrement'],
        ['TAG_IMAGE', 'develop']
    ]);
    switch (nomModule) {
        case 'fichiers-conversions-audiovideo': {
            params.set('MEMORY_LIMIT', '2Gi');
            break;
        }
        case 'fichiers': {
            if (profilsSpring.includes('docgen')) {
                params.set('CPU_REQUEST', '1000m');
            } else {
                //Puisqu'il était pas facile de passer la ligne de commande au shell à cause des différents caractères séciaux, on efface la cmd plutot que de la passer
                params.set('LIBREOFFICE_CMD', '');
            }
            break;
        }
    }
    if (envName === 'develop') {
        params.set('NOMBRE_REPLICAS', '2');
        params.set('SYSDIG_FLAG', '-Dflag.sysdig.ca.ulaval.ena2');
        params.set('JMX_ACTIVATION', '-Dcom.sun.management.jmxremote.port=7091');
    } else {
        params.set('READINESS_URL', 'liveness');
    }
    let listeParamsTemplate = construireListeParamsValue(params);
    listeParamsTemplate = listeParamsTemplate ? `-p ${listeParamsTemplate}` : ``;
    return listeParamsTemplate;
}
