/** Script qui permet di'initialiser le bucket S3 ENA2 si nécessaire
 * Crée le bucketS3. Sinon, recharge les données
 * Ex. deno run --allow-read --allow-run --allow-net initS3.ts --token=$(oc whoami -t) 
 *
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as utils from "../pipeline-utils.ts";
import { AwsConfig } from "../aws/awsconfig.ts";
import * as s3 from "../aws/s3.ts";

const { endpointUrlS3, accessKeyIdS3, accessKeySecretS3, rechargerDonnees} = parse(Deno.args, {
    boolean: ["rechargerDonnees"],
    default: {
        endpointUrlS3: "https://s3.dev.brioeducation.ca",
        rechargerDonnees: false,
    }
});

if (!accessKeyIdS3 || !accessKeySecretS3) {
    throw new Error("Les credentials S3 sont obligatoires");
}

const currentWorkingDirectory = Deno.cwd();
await utils.doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});

const nomBranche = await utils.doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});
const envName = utils.getEnvName(nomBranche);

console.log(`Initialisation pour env ${envName} (branche git ${nomBranche}) `);

const isBrancheFeature = new RegExp('^((?!develop|main|master|ena2-4385).)*$').test(envName);
//Valider le nom de la branche
if (!isBrancheFeature) {
    console.warn("Impossible d'initialiser les environnements permanents");
    Deno.exit(0);
}

const configAWS: AwsConfig = {
    accessKeyId: accessKeyIdS3,
    accessKeySecret: accessKeySecretS3,
    region: 'ca-central-1',
    url: endpointUrlS3
};

console.log(`Forcer un rechargement? ${rechargerDonnees}`);

await s3.initS3(configAWS, nomBranche, rechargerDonnees);
