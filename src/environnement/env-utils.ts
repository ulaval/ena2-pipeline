/** 
 * Utilitaire qui vient en support à l'initialisation et au déploiement de l'environnement des applcations ENA2
 */
import { ConfigOpenshift } from "../openshift.ts";
import * as oc from "../openshift.ts";
import { doExecute, doExecuteWithStdout, getEnvName, sleep } from "../pipeline-utils.ts";

export interface OptionsDeploiement {
  choix: string;
  descriptions: string;
}

export async function determinerOptionsDeploiements(nomService: string, configOC: ConfigOpenshift, deploiementCompletAutorise: boolean, isAppExiste: (config: ConfigOpenshift, nomApp: string) => Promise<boolean>): Promise<string> {
  const isDevelop = new RegExp('^(develop)-.*$').test(nomService);
  const isBrancheFeature = new RegExp('^((?!develop|main|master|ena2-4385).)*$').test(nomService);
  const isServiceExiste: boolean = (await isAppExiste(configOC, `${nomService}`)) || !isBrancheFeature;

  const descSansDeploiement = 'RIEN: Aucune opération en lien avec les pods.\n';
  const descAvecDeploiement = 'DEPLOIEMENT: Déploie sur openshift en mode "fusionnés" (pod web et tache ensemble).\n';
  const descAvecDeploiementSansCR = 'DEPLOIEMENT: Déploie sur openshift en mode "fusionnés" (pod web et tache ensemble).';
  const descAvecDeploiementComplet = deploiementCompletAutorise ? 'DEPLOIEMENT_COMPLET: Déploie les pods de type taches séparément.\n' : '';
  const descAvecDeploiementCompletSansCR = deploiementCompletAutorise ? '\nDEPLOIEMENT_COMPLET: Déploie les pods de type taches séparément.' : '';
  const descDemarrerPods = 'DEMARRER_PODS: Démarre les pods pour cet environnement.\n';
  const descFermerPods = 'FERMER_PODS: Ferme les pods pour cet environnement.\n';
  const choixDeploiementComplet = deploiementCompletAutorise ? '\nDEPLOIEMENT_COMPLET' : '';

  let choix = '';
  let description = '';
  if (isDevelop) {
    choix = deploiementCompletAutorise ? 'DEPLOIEMENT_COMPLET\nRIEN' : 'DEPLOIEMENT\nRIEN';
    description = deploiementCompletAutorise ? descAvecDeploiementComplet + descSansDeploiement : descAvecDeploiement + descSansDeploiement;
  } else if (isBrancheFeature) {
    if (isServiceExiste) {
      choix = 'RIEN\nDEPLOIEMENT' + choixDeploiementComplet + '\nDEMARRER_PODS\nFERMER_PODS';
      description = descSansDeploiement + descAvecDeploiement + descAvecDeploiementComplet + descDemarrerPods + descFermerPods;
    } else {
      choix = 'RIEN\nDEPLOIEMENT' + choixDeploiementComplet;
      description = descSansDeploiement + descAvecDeploiementSansCR + descAvecDeploiementCompletSansCR;
    }
  } else {
    choix = 'DEPLOIEMENT\nRIEN' + choixDeploiementComplet + '\nDEMARRER_PODS\nFERMER_PODS';
    description = descAvecDeploiement + descSansDeploiement + descAvecDeploiementComplet + descDemarrerPods + descFermerPods;
  }

  const optionsDeploiement: OptionsDeploiement = {
    choix: choix,
    descriptions: description
  };
  return JSON.stringify({ optionsDeploiement, isServiceExiste, isBrancheFeature });
}

export async function ajusterPods(configOC: ConfigOpenshift, nombreReplicas: number, nomEnvironnement: string, stopCondition: string) {
  const labels = new Map<string, string>([
    ['app', nomEnvironnement]
  ]);
  // develop a besoin de plus de temps pour ajuster les pods, car plus de pods sont démarrés
  const maxTentatives = nomEnvironnement == 'develop' ? 600 : 300;

  await oc.scale(configOC, nombreReplicas, labels);

  let index = 0;
  while (await oc.getNbPods(configOC, labels, stopCondition) != 0) {
    if (index++ >= maxTentatives) {
      // En cas de problème, effectue un scale down du service pour ne pas épuiser les ressources avec des CrashLoopBackOff
      await oc.scale(configOC, 0, labels);
      throw new Error("Impossible d'ajuster les pods (redémarrer ou fermer).");
    }
    await sleep(1000);
  }
}

export async function getNomEnvironnement(): Promise<string> {
  const currentWorkingDirectory = Deno.cwd();
  await doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
  });

  const branchName = await doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
  });

  return getEnvName(branchName);
}