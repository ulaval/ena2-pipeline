import { Cron } from "https://deno.land/x/croner@5.1.0/src/croner.js";
import { format } from "https://deno.land/std@0.188.0/datetime/mod.ts";

const nextCron = new Cron("0 0 * * 2,4").next();

if (!nextCron) {
    throw new Error("Date impossible à déterminer");
}

const prochaineDate = format(nextCron, "yy-MM-dd");
console.log(prochaineDate);
