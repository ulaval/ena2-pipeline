/** Script qui permet d'archiver les dossiers de migrations, créer un nouveau répertoire et de soumettre dans git
 * Ex. deno run --allow-run --allow-read archiver-migrations.ts --gitUser=user
 *  --gitPassword=password gitEmail=mathieu.toussaint@dti.ulaval.ca --nouveauRepMigration=23.09.03
 */

import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import { doExecute, doExecuteWithStdout } from "../pipeline-utils.ts";
import { format } from 'https://deno.land/std@0.106.0/datetime/mod.ts';

const { nomService, gitPassword, gitUser, gitEmail, nouveauRepMigration, brancheDonnees } = parse(Deno.args, {
  default: {
    gitUser: "ena2_usager",
    gitEmail: "ena2_usager@dti.ulaval.ca",
    brancheDonnees: "feature/ENA2-4385_jeu-de-donnees"
  },
});

//Valider les paramètre
if (!gitPassword || !gitUser) {
  throw new Error("Les credentials de git/bitbucket sont obligatoires");
}


if (!nomService) {
  throw new Error("Le nom du service est obligatoires (nomService)");
}

const currentWorkingDirectory = Deno.cwd();
Deno.chdir(`${currentWorkingDirectory}/${nomService}-bd/migrations`); // Change le workdir

if (await migrationsVide()) {
  //Rien à faire
  console.log("Aucune migration");
  Deno.exit(0);
}

await doExecute("git", {
  args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});

const nowDate = format(new Date(), 'yy.MM.dd');

await doExecute("mkdir", {
  args: ["-p", `./-merged/${nowDate}`]
});

//Déplacer les répertoires ena2-bd/migrations/* (sauf ceux qui commencent par -)  dans le répertoire -merged/nowdate   
await doExecute("bash", {
  args: ["-c", `find . -maxdepth 1 -type d  |grep -E '^./[^-].*' | xargs -i sh -c  'cp -r {} -t -merged/${nowDate} ; rm -rf {}'`]
});

Deno.chdir(`${currentWorkingDirectory}`); // Change le workdir
const gitCred = `${gitUser}:${gitPassword}`;
await doExecute("git", { args: ['add', '.'] });
await doExecute("git", {
  args: [
    '-c',
    `user.name='${gitUser}'`,
    '-c',
    `user.email='${gitEmail}'`,
    'commit',
    '-am',
    `Merge à base`,
  ],
});

const gitRepositoryBE = `https://${gitCred}@github.com/universite-laval/ul-${nomService}.git`;
await doExecute("git", { args: ['push', gitRepositoryBE] });
await doExecute("git", { args: ['checkout', 'develop'] });
await doExecute("git", { args: ['pull', gitRepositoryBE, "--rebase"] });
await doExecute("git", {
  args: [
    '-c',
    `user.name='${gitUser}'`,
    '-c',
    `user.email='${gitEmail}'`,
    'merge',
    '--no-ff',
    '--no-commit',
    `${brancheDonnees}`,
  ],
});

await doExecute("git", {
  args: [
    '-c',
    `user.name='${gitUser}'`,
    '-c',
    `user.email='${gitEmail}'`,
    'commit',
    '-am',
    `Merge à base`,
  ],
});

await await doExecute("git", { args: ['push', gitRepositoryBE] });


async function migrationsVide(
): Promise<boolean> {
  let result = "";
  try {
    result = await doExecuteWithStdout("bash", {
      args: ["-c", "find . -maxdepth 1 -type d  |grep -E '^./[^-].*' | wc -l"]
    });

  } catch (err) {
    console.log("Error:" + err.message);
    return false;
  }
  return result.trim() === ("0");
}

