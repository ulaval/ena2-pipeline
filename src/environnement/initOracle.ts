/** Script qui permet di'initialiser la BD Oracle ENA2 si nécessaire
 * Crée la BD oracle. Sinon, recharge les données
 * Ex. deno run --allow-read --allow-run --allow-net initOracle.ts
 *
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import { OracleConfig } from '../oracle/oracleConfig.ts';
import * as oracle from "../oracle/oracle.ts";
import { getNomEnvironnement } from "./env-utils.ts";


const { rechargerDonnees,
    nomUtilisateurOracle, motDePasseOracle, nomHoteOracle, portOracle, serviceOracle,
    mavenSettingsPath, mavenProfil } = parse(Deno.args, {
        boolean: ["rechargerDonnees"],
        default: {
            rechargerDonnees: false,
            nomHoteOracle: 'exaciidv-scan.x.ul.ca',
            portOracle: '1521',
            serviceOracle: 'e2dv.ul.ca'
        }
    });

if (!nomUtilisateurOracle || !motDePasseOracle) {
    throw new Error("Les credentials Oracle sont obligatoires");
}

if (!mavenSettingsPath || !mavenProfil) {
    throw new Error("Les config maven sont obligatoires");
}

const envName: string = await getNomEnvironnement();

console.log(`Initialisation pour env ${envName} `);

const isBrancheFeature = new RegExp('^((?!develop|main|master|ena2-4385).)*$').test(envName);
//Valider le nom de la branche
if (!isBrancheFeature) {
    console.warn("Impossible d'initialiser les environnements permanents");
    Deno.exit(0);
}

const oracleConfig: OracleConfig = {
    nomUtilisateur: nomUtilisateurOracle,
    motDePasse: motDePasseOracle,
    hoteOracle: nomHoteOracle,
    portOracle: portOracle,
    serviceOracle: serviceOracle
};

console.log(`Forcer un rechargement? ${rechargerDonnees}`);
await oracle.initBD(oracleConfig, envName, mavenSettingsPath, mavenProfil, rechargerDonnees);
