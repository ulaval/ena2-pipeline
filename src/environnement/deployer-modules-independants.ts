/** Script qui permet de déployer les services d'ENA2 si nécessaire
 *
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as utils from "../pipeline-utils.ts";
import { ConfigOpenshift } from "../openshift.ts";
import * as services from "./services.ts";

const { server, token, namespace, deployerLti, deployerSynchroEtudes, deployerAgregateur, modeDeploiement } = parse(Deno.args, {
    boolean: ["deployerLti", "deployerSynchroEtudes", "deployerAgregateur"],
    string: ["modeDeploiement"],
    default: {
        server: "https://api.ul-pca-pr-ul02.ulaval.ca:6443",
        namespace: "ul-ena2-dv01",
        deployerLti: false,
        deployerSynchroEtudes: false,
        deployerAgregateur: false,
        modeDeploiement: 'RIEN'
    }
});

//Valider les paramètre
if (!token) {
    throw new Error("Le paramètre token est obligatoire");
}

const currentWorkingDirectory = Deno.cwd();
await utils.doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});

const nomBranche = await utils.doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});
const envName = utils.getEnvName(nomBranche);

console.log(`Déploiement modules indépendant pour env ${envName} (branche git ${nomBranche}) `);

const isBrancheFeature = new RegExp('^((?!develop|main|master|ena2-4385).)*$').test(envName);
//Valider le nom de la branche
if (!isBrancheFeature) {
    console.warn("Impossible d'initialiser les environnements permanents");
    Deno.exit(0);
}

const modulesADeployer = ['audit'];
if (deployerAgregateur) {
    modulesADeployer.push('agregateur');
}

if (deployerLti) {
    modulesADeployer.push('lti');
}

if (deployerSynchroEtudes) {
    modulesADeployer.push('synchro-etudes');
}

//Déployer les services indépendants
const configOC: ConfigOpenshift = { server: server, namespace: namespace, token: token };
services.deployerServicesSpecifiques(configOC, modulesADeployer, modeDeploiement);
