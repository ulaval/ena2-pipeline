/** Script qui vient en support aux manipulations de l'environnement des applcations ENA2 si nécessaire
 * Ex. deno run --allow-read --allow-run --allow-net env-actions.ts --token=$(oc whoami -t) 
 *
 */
import { parseArgs } from "https://deno.land/std@0.207.0/cli/parse_args.ts";
import { Cron } from "https://deno.land/x/croner@5.1.0/src/croner.js";
import { format } from "https://deno.land/std@0.207.0/datetime/mod.ts";
import { ConfigOpenshift } from "../openshift.ts";
import * as oc from "../openshift.ts";
import { ajusterPods, determinerOptionsDeploiements, getNomEnvironnement } from "./env-utils.ts";
import { getEnvName } from "../pipeline-utils.ts";

const actions: string[] = ["VerifierServiceExiste", "VerifierTransfertLogsActive", "ActiverTransfertLogs", "DeterminerProchaineDateLivraison", "ExtraireNomEnvironnement", "DeterminerOptionsDeploiement", "DemarrerPods", "FermerPods"];

/**
 * nomService doit avoir la forme <idenv>-<nomservice>, ex: ena2-10220-communications
 */
const { server, token, namespace, action, nomService, deploiementCompletAutorise, nomBranche}: {server: string, token: string, namespace: string, action: string, nomService: string, deploiementCompletAutorise: boolean, nomBranche?: string} = parseArgs(Deno.args, {
    boolean: ["deploiementCompletAutorise"],
    default: {
        server: "https://api.ul-pca-pr-ul02.ulaval.ca:6443",
        namespace: "ul-ena2-dv01",
        deploiementCompletAutorise: false
    },
    string: ["server", "token", "namespace", "action", "nomService", "nomBranche"]
});

//Valider les paramètres
if (!token) {
    throw new Error("Le paramètre token est obligatoire.");
}

if (!action) {
    throw new Error("Le paramètre action est obligatoire.");
}

//valider action possible
if (!actions.includes(action)) {
    throw new Error(action + " n'est pas une action valide");
}

const configOC: ConfigOpenshift = { server: server, namespace: namespace, token: token };

if (action === "VerifierServiceExiste") {
    const existe: boolean = (await oc.isPodExiste(configOC, `${nomService}`));
    console.log(existe);
    Deno.exit(0);
}

if (action === "VerifierTransfertLogsActive") {
    const existe: boolean = (await oc.isPodTagForwardLogsCloud(configOC, `${nomService}`, 'ul-ena2-npr'));
    console.log(existe);
    Deno.exit(0);
}

if (action === "ActiverTransfertLogs") {
    const envName = await getNomEnvironnement()

    const labelsPods = new Map<string, string>([
        ["app", envName],
    ]);

    await oc.labelPods(configOC, labelsPods, 'forwardLogsCloud', 'ul-ena2-npr');

    Deno.exit(0);
}

if (action === "ExtraireNomEnvironnement") {
    if (!nomBranche) {
        throw new Error("Le paramètre nomBranche est obligatoire.");
    }
    console.log(getEnvName(nomBranche));

    Deno.exit(0);
}

if (action === "DeterminerProchaineDateLivraison") {
    const nextCron = new Cron("0 0 * * 2,4").next();

    if (!nextCron) {
        throw new Error("Date impossible à déterminer");
    }

    const prochaineDate = format(nextCron, "yy-MM-dd");
    console.log(prochaineDate);

    Deno.exit(0);
}

if (action === "DeterminerOptionsDeploiement") {
    const resultat: string = await determinerOptionsDeploiements(nomService, configOC, deploiementCompletAutorise, oc.isAppExiste);

    console.log(resultat);

    Deno.exit(0);
}

if (action === "DemarrerPods") {
    if (!nomBranche) {
        throw new Error("Le paramètre nomBranche est obligatoire.");
    }
    const nomEnvironnement: string = getEnvName(nomBranche);
    const nombreReplicas = nomEnvironnement == 'develop' ? 2 : 1;

    await ajusterPods(configOC, nombreReplicas, nomEnvironnement, "status.phase!=Running");

    Deno.exit(0);
}

if (action === "FermerPods") {
    if (!nomBranche) {
        throw new Error("Le paramètre nomBranche est obligatoire.");
    }
    const nomEnvironnement: string = getEnvName(nomBranche);
    await ajusterPods(configOC, 0, nomEnvironnement, "status.phase!=Terminating");

    Deno.exit(0);
}
