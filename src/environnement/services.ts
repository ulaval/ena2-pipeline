/** Script qui permet de déployer un environnement de dev en déployant tous les app d'ENA2, selon l'id de la branche GIT
 * Ex. deno run --allow-read --allow-run --allow-net deployApplications.ts
 * --artifactoryMotDePasse=password --artifactoryUtilisateur=username
 */
import * as utils from "../pipeline-utils.ts";
import * as openshift from "../openshift.ts";
import { ConfigOpenshift } from "../openshift.ts";
import * as oracle from "../oracle/oracle.ts";
import * as templatesUtils from "./templates-utils.ts";

export interface ServiceDetails {
    nomService: string;
    nomRepo: string;
    profilsSpring: string;
    nomTemplate?: string;
}

// Les services optionnels sont déployables, mais ne sont pas requis pour chaque environnement
// Ce sont des services qui n'ont jamais fait parti de ena2-module pour être déployé avec les 
// autres de façon automatique.
const servicesOptionnels: ServiceDetails[] = [
  { nomService: 'agregateur', nomRepo: 'ena2-agregateur', profilsSpring: '' },
  { nomService: 'lti', nomRepo: 'ena2-lti', profilsSpring: 'web,taches' },
  { nomService: 'synchro-etudes', nomRepo: 'ena2-synchro-etudes', profilsSpring: 'web,taches' }
]

const servicesFusionnes: ServiceDetails[] = [
    { nomService: 'identites', nomRepo: 'ena2-identites', profilsSpring: 'web,taches' },
    { nomService: 'audit', nomRepo: 'ena2-audit', profilsSpring: 'web,taches' },
    { nomService: 'communications', nomRepo: 'ena2-communications', profilsSpring: 'web,taches' },
    { nomService: 'fichiers', nomRepo: 'ena2-fichiers', profilsSpring: 'web,taches,conversionAudioVideo,docgen' },
    { nomService: 'formations', nomRepo: 'ena2-formations', profilsSpring: 'web,taches' },
    { nomService: 'paiements', nomRepo: 'ena2-paiements', profilsSpring: 'web,taches' },
    { nomService: 'questionnaires', nomRepo: 'ena2-questionnaires', profilsSpring: 'web,taches' },
    { nomService: 'sitepromo', nomRepo: 'ena2-site-promo', profilsSpring: 'web,taches' },
    { nomService: 'sites', nomRepo: 'ena2-sites', profilsSpring: 'web,taches' },
    { nomService: 'recherches', nomRepo: 'ena2-recherches', profilsSpring: 'web,taches' },
    { nomService: 'coordonnateur', nomRepo: 'ena2-coordonnateur', profilsSpring: 'web,taches' }
]

const servicesComplet: ServiceDetails[] = [
    { nomService: 'identites', nomRepo: 'ena2-identites', profilsSpring: 'web' },
    { nomService: 'identites-taches', nomRepo: 'ena2-identites', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'audit', nomRepo: 'ena2-audit', profilsSpring: 'web' },
    { nomService: 'audit-taches', nomRepo: 'ena2-audit', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'communications', nomRepo: 'ena2-communications', profilsSpring: 'web' },
    { nomService: 'communications-taches', nomRepo: 'ena2-communications', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'fichiers', nomRepo: 'ena2-fichiers', profilsSpring: 'web' },
    { nomService: 'fichiers-taches', nomRepo: 'ena2-fichiers', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'fichiers-conversions-audiovideo', nomRepo: 'ena2-fichiers', profilsSpring: 'conversionAudioVideo', nomTemplate: 'template-application-conversion-audiovideo.yaml' },
    { nomService: 'fichiers-docgen', nomRepo: 'ena2-fichiers', profilsSpring: 'docgen', nomTemplate: 'template-application-docgen.yaml' },
    { nomService: 'formations', nomRepo: 'ena2-formations', profilsSpring: 'web' },
    { nomService: 'formations-taches', nomRepo: 'ena2-formations', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'paiements', nomRepo: 'ena2-paiements', profilsSpring: 'web' },
    { nomService: 'paiements-taches', nomRepo: 'ena2-paiements', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'questionnaires', nomRepo: 'ena2-questionnaires', profilsSpring: 'web' },
    { nomService: 'questionnaires-taches', nomRepo: 'ena2-questionnaires', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'sitepromo', nomRepo: 'ena2-site-promo', profilsSpring: 'web' },
    { nomService: 'sitepromo-taches', nomRepo: 'ena2-site-promo', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'sites', nomRepo: 'ena2-sites', profilsSpring: 'web' },
    { nomService: 'sites-taches', nomRepo: 'ena2-sites', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'recherches', nomRepo: 'ena2-recherches', profilsSpring: 'web' },
    { nomService: 'recherches-taches', nomRepo: 'ena2-recherches', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' },
    { nomService: 'coordonnateur', nomRepo: 'ena2-coordonnateur', profilsSpring: 'web' },
    { nomService: 'coordonnateur-taches', nomRepo: 'ena2-coordonnateur', profilsSpring: 'taches', nomTemplate: 'template-application-taches.yaml' }
]

export async function deployerServices(configOC: ConfigOpenshift, exlcusion: string[] = []) {
    const currentWorkingDirectory = Deno.cwd();

    await utils.doExecute("git", {
        args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
    });

    const branchName = await utils.doExecuteWithStdout("git", {
        args: ["branch", "--show-current"]
    });

    const envName = utils.getEnvName(branchName);
    const nomSchemaOracle = oracle.genererNomSchemaOracle(branchName);
    const envNameSansTiret = envName.replace('-', '.');

    console.log("Tous les services seront déployés, à l'exception de: " + exlcusion);

    //filtrer la liste en enlevant le service courant car il sera déployer par son propre pipeline
    const servicesFiltered = servicesFusionnes.filter((service) => !exlcusion.includes(service.nomService));

    //Déployer les configmaps
    servicesFiltered.forEach(async (service) => await deployerConfigmap(envName, branchName, service, envNameSansTiret, nomSchemaOracle, configOC));
    //Déployer les services
    servicesFiltered.forEach(async (service) => await deployerService(envName, branchName, service, configOC));
}

export async function deployerServicesSpecifiques(configOC: ConfigOpenshift, services: string[], modeDeploiement: string) {
    const currentWorkingDirectory = Deno.cwd();

    await utils.doExecute("git", {
        args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
    });

    const branchName = await utils.doExecuteWithStdout("git", {
        args: ["branch", "--show-current"]
    });

    const envName = utils.getEnvName(branchName);
    const nomSchemaOracle = oracle.genererNomSchemaOracle(branchName);
    const envNameSansTiret = envName.replace('-', '.');

    console.log("Déploiement des services: " + services);

    //filtrer la liste en enlevant le service courant car il sera déployer par son propre pipeline
    const servicesADeployer = construireServicesADeployer(services, modeDeploiement);

    //Déployer les configmaps
    servicesADeployer.forEach(async (service) => await deployerConfigmap(envName, branchName, service, envNameSansTiret, nomSchemaOracle, configOC));
    //Déployer les services
    servicesADeployer.forEach(async (service) => await deployerService(envName, branchName, service, configOC));
}

async function deployerConfigmap(envName: string, branchName: string, service: ServiceDetails, envNameSansTiret: string, nomSchemaOracle: string, configOC: ConfigOpenshift) {
  const labelsTemplate = templatesUtils.construireLabelsModuleADeployer(envName, branchName, service.nomRepo);
  const paramsConfigMap = templatesUtils.construireParamsConfigMap(service.nomRepo, service.nomService, envName, envNameSansTiret, nomSchemaOracle);

  console.log(`deploy configmap : ` + JSON.stringify(service));

  //Mise à jour/création du configmap
  await utils.doExecute(
    'docker',
    {
      args: [
        "run",
        "--pull=always",
        "-t",
        `docker-local.maven.ulaval.ca/enseignement/ena2/${service.nomRepo}-deploy-dv:latest`,
        "/bin/bash",
        "-c",
        "oc process " +
        `--server=${configOC.server} ` +
        `--token=${configOC.token} ` +
        `--namespace=${configOC.namespace} ` +
        `${paramsConfigMap} ` +
        `${labelsTemplate} ` +
        `-f openshift/template-configmap.yaml` +
        '| oc apply ' +
        `--server=${configOC.server} ` +
        `--token=${configOC.token} ` +
        `--namespace=${configOC.namespace} ` +
        `-f -`
      ]
    });
}

async function deployerService(envName: string, branchName: string, service: ServiceDetails, configOC: ConfigOpenshift) {
  const labelsTemplate = templatesUtils.construireLabelsModuleADeployer(envName, branchName, service.nomRepo);
  const paramsDeploymentConfig = templatesUtils.construireParamsDeploymentConfig(service.nomRepo, envName, service.profilsSpring);

  console.log(`deploy service app : ` + JSON.stringify(service));

  //Mise à jour/création du deploymentconfig
  await utils.doExecute(
    'docker',
    {
      args: [
        "run",
        "--pull=always",
        "-t",
        `docker-local.maven.ulaval.ca/enseignement/ena2/${service.nomRepo}-deploy-dv:latest`,
        "/bin/bash",
        "-c",
        "oc process " +
        `--server=${configOC.server} ` +
        `--token=${configOC.token} ` +
        `--namespace=${configOC.namespace} ` +
        `${paramsDeploymentConfig} ` +
        `${labelsTemplate} ` +
        `-f openshift/template-application.yaml` +
        '| oc apply ' +
        `--server=${configOC.server} ` +
        `--token=${configOC.token} ` +
        `--namespace=${configOC.namespace} ` +
        `-f -`
      ]
    });

  const dcName = envName + `-` + service.nomService;
  console.log(`dcName : ${dcName}`);
  await openshift.rolloutLatestAndWatch(configOC, dcName);
}

export async function isServiceExiste(
    config: ConfigOpenshift,
    nomRessource: string
): Promise<boolean> {
    return await openshift.isPodExiste(config, `${nomRessource}`);
}

export function getDetailsServiceCourant(serviceCourant: string, modeDeploiement: string): ServiceDetails[] {
  console.log(`obtenir détails pour ${serviceCourant} :`);
  // Pour les services optionnels qui n'ont qu'un mode de déploiement, on les retourne directement
  if (servicesOptionnels.some((service) => service.nomService.includes(serviceCourant))) {
    console.log(`${serviceCourant} est optionnel`);
    return servicesOptionnels.filter((service) => service.nomService.includes(serviceCourant));
  }

  // Retourne les détails du service courant selon le mode de déploiement
  if (modeDeploiement === 'DEPLOIEMENT_COMPLET') {
      return servicesComplet.filter((service) => service.nomService.includes(serviceCourant));
  }
  return servicesFusionnes.filter((service) => service.nomService.includes(serviceCourant));
}

function construireServicesADeployer(services: string[], modeDeploiement: string) : ServiceDetails[] {
  let servicesADeployer: ServiceDetails[] = [];
  services.forEach((service) => servicesADeployer = [...servicesADeployer, ...getDetailsServiceCourant(service, modeDeploiement)]);
  return servicesADeployer;
}
