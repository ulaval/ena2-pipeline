/** Script qui permet de déployer ;es services d'ENA2 si nécessaire
 *
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as utils from "../pipeline-utils.ts";
import { ConfigOpenshift } from "../openshift.ts";
import * as services from "./services.ts";

const { server, token, namespace, forcerInstallation, serviceCourant } = parse(Deno.args, {
    boolean: ["forcerInstallation"],
    default: {
        server: "https://api.ul-pca-pr-ul02.ulaval.ca:6443",
        namespace: "ul-ena2-dv01",
        forcerInstallation: false
    }
});

//Valider les paramètre
if (!token) {
    throw new Error("Le paramètre token est obligatoire");
}

const configOC: ConfigOpenshift = { server: server, namespace: namespace, token: token };


const currentWorkingDirectory = Deno.cwd();
await utils.doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});

const nomBranche = await utils.doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});
const envName = utils.getEnvName(nomBranche);

console.log(`Initialisation pour env ${envName} (branche git ${nomBranche}) `);

const isBrancheFeature = new RegExp('^((?!develop|main|master|ena2-4385).)*$').test(envName);
//Valider le nom de la branche
if (!isBrancheFeature) {
    console.warn("Impossible d'initialiser les environnements permanents");
    Deno.exit(0);
}

const serviceExiste = await services.isServiceExiste(configOC, `${envName}-sites`);

console.log(`Forcer un installation? ${forcerInstallation}`);

//Déployer les autres services ena2
if (!serviceExiste || forcerInstallation) {
    services.deployerServices(configOC, [serviceCourant]);
}