import * as utils from "./pipeline-utils.ts";

/**
 * @param url URL du repo, incluant le nom d'utilisateur et le mot de passe. https://${username}:${password}@bitbucket.org/ulaval/${repoName}
 * @param branch Branche git à utiliser. master, develop, ena2-1234
 * Ref: Deuxième réponse de https://stackoverflow.com/questions/29368837/copy-a-git-repo-without-history
 */
export interface ConfigGit {
  rootDir: string;
  repo: string;
  credentials: string;
  nom: string;
  courriel: string;
}

export async function checkout(
  rootDir: string,
  url: string,
  branch: string,
) {
  const output = await utils.doExecute('git', {
    cwd: rootDir,
    args: [
      'clone',
      '--depth', '1',
      '--branch', `${branch}`,
      '--',
      url
    ],
  });

  console.log(`Output: ${output}`);
}

export async function tag(config: ConfigGit, nomTag: string, descriptionTag: string) {
  await utils.doExecute('git', {
    cwd: config.rootDir,
    args: [
      '-c', `user.name=${config.nom}`,
      '-c', `user.email=${config.courriel}`,
      'tag',
      '-a', nomTag,
      '-m', descriptionTag
    ],
  });

  await utils.doExecute('git', {
    cwd: config.rootDir,
    args: [
      'push',
      `https://${config.credentials}@github.com/universite-laval/ul-${config.repo}.git`,
      '--tags'
    ],
  });
}