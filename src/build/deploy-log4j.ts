import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as openshift from "../openshift.ts";
import { doExecute, doExecuteWithStdout, getDescriptifEnv, getEnvName } from "../pipeline-utils.ts";

const { server, token, namespace, modeDeploiement, urlTemplateLog4j } = parse(Deno.args, {
    boolean: ["avecMigrationsBD", "activerTransfertLogs"],
    default: {
        server: "https://api.ul-pca-pr-ul02.ulaval.ca:6443",
        namespace: "ul-ena2-dv01",
        avecMigrationsBD: true,
        activerTransfertLogs: false,
        extraConfigmapParams: "",
        modeDeploiement: 'RIEN'
    }
})

// Valider les paramètre
if (modeDeploiement === 'RIEN') {
    // On ne devrait pas se trouver ici pour un build sans déploiement, mais on valide en cas d'erreur dans le Jenkinsfile qui appel.
    throw new Error("Le build est demandé sans déploiement. Aucun déploiement ne sera fait.");
}

if (!token) {
    throw new Error("Le paramètre token est obligatoire");
}

const currentWorkingDirectory = Deno.cwd();
await doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});

const branchName = await doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});

const envName = getEnvName(branchName);

//D'abord le log4j
const labels = new Map<string, string>();
labels.set('app', envName)
    .set('description', getDescriptifEnv(branchName));
const params = new Map<string, string>([
    ['NOM_ENVIRONNEMENT', envName]
]);
await openshift.apply({ server: server, namespace: namespace, token: token }, urlTemplateLog4j, labels, params);
console.log(`Configmap Log4J déployé pour l'env. ${envName}`);