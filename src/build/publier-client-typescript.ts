/** Script qui permet de publier ena2-audit-api si nécessaire
 *  Le numéro de version de l'artifact publié peut être spécifié avec le paramètre numeroVersion, sinon il sera calculé automatiquement
 * Ex. deno run --allow-run scripts/publier-api.ts --artifactoryUsername=${username} --artifactoryPassword=${mot de passe encrypté} --numeroVersion=${optionel}
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as utils from "../pipeline-utils.ts";
import * as artifactory from "../artifactory.ts";
import { tag } from "../git.ts";


const { nomArtifact, repoArtifact, artifactoryUtilisateur, artifactoryMotDePasse, 
        buildCommit, previousSuccesfulBuildCommit, gitCredentials, gitUtilisateur, gitCourriel,
        numeroVersion, forcerPublication, fichiersSurveilles, validerClientTS } = parse(Deno.args, {
    boolean: ["forcerPublication", "validerClientTS"]
});


const currentWorkingDirectory = Deno.cwd();
console.log("currentWorkingDirectory: " + currentWorkingDirectory);


await utils.doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});


const branchName = await utils.doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});

if (!nomArtifact || !repoArtifact) {
    throw new Error("Les infos sur l'artifact à publier sont obligatoires");
}

if (!artifactoryUtilisateur || !artifactoryMotDePasse) {
    throw new Error("Les credentials artifactory sont obligatoires");
}

console.log("branchName: " + branchName);
console.log("buildCommit : " + buildCommit);
console.log("previousSuccesfulBuildCommit : " + previousSuccesfulBuildCommit);

const repertoiresSourcesModifies = await utils.doExecuteWithStdout("bash", {
    args: ["-c", "git diff --name-only " + buildCommit + " " + previousSuccesfulBuildCommit + " | awk '{split($0,a,\"/\"); print a[1];}'| sort -u | uniq"]
});
console.log("repertoiresSourcesModifies : " + repertoiresSourcesModifies);

const envName = utils.getEnvName(branchName);


/**
 * Publication de la prochaine version d'un artifact:
 * Pour la branche develop:
 *      - Le code des fichiers/répertoires surveillés sont modifié.  Le numéro de version mineure est incrémentée automtiquement.
 *      - Un numéro de version peut être spécifié en paramètre de la tâche jenkins
 * Pour les autres branches :
 *       - Une version de test peut être publiée en cochant le paramètre jenkins : PUBLIER_API (forcerPublication).
 */
let versionPublication = numeroVersion;

const fichiersSurveillesArray: string[] = fichiersSurveilles ? fichiersSurveilles.split(",") : [];
let artifactModifie = false;
fichiersSurveillesArray.forEach((fichierSurveille) => {
    if (repertoiresSourcesModifies.includes(fichierSurveille)) {
        artifactModifie = true;
    }
});

if (branchName == "develop" && artifactModifie && !versionPublication) {
    // Calculer le prochain numéro de version s'il n'est pas défini
    if (!versionPublication) {
        const versions = await artifactory.obtenirVersionsExistantes(repoArtifact, repoArtifact, artifactory.TypeArtifact.Npm, artifactoryUtilisateur, artifactoryMotDePasse);
        versionPublication = artifactory.nouvelleVersionSemVer(versions);
    }
}

// Autres branches: déterminer le numéro de version de test si nécessaire
if (forcerPublication && branchName != "develop") {
    const commitCourt = await utils.doExecuteWithStdout("git", {
        args: ["rev-parse", "--short", "HEAD"]
    });
    versionPublication = "1.0.0-" + envName + "-" + commitCourt;
}

if (!versionPublication && !validerClientTS) {
    console.warn("Client non publié, aucune version fournie");
    Deno.exit(0);
}

// Création et push du tag (seulement sur develop)
if (branchName == "develop" && !validerClientTS) {
    tag({repo: nomArtifact, credentials: gitCredentials, rootDir: currentWorkingDirectory, nom: gitUtilisateur, courriel: gitCourriel},
        `client-${versionPublication}`,
        `Tag client typescript ${versionPublication}`)
}

// Generer les typescripts
await utils.doExecute("bash", {

    //args: ["-c", "mvn install -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -Dmaven.test.skip=true -PgenerationTS -f ../pom.xml $MAVEN_CONFIG"]
    args: ["-c", "mvn clean install -Dmaven.test.skip=true -PgenerationTS -f ./pom.xml $MAVEN_CONFIG"]

});

// Compiler et publier les client typescripts
await utils.doExecute("bash", { args: ["-c", `npm --prefix ./${nomArtifact}-client-typescript install`] });
await utils.doExecute("bash", { args: ["-c", `npm --prefix ./${nomArtifact}-client-typescript run clean`] });
await utils.doExecute("bash", { args: ["-c", `npm --prefix ./${nomArtifact}-client-typescript run tsc`] });

// Publie seulement si pas en mode validation du client typescript
if (!validerClientTS) {
    console.log(`Publication de la version de ${nomArtifact} :  ${versionPublication}`);
    await utils.doExecute("bash", { args: ["-c", `npm --prefix ./${nomArtifact}-client-typescript version ${versionPublication}`] });
    await utils.doExecute("bash", { args: ["-c", `npm publish ./${nomArtifact}-client-typescript`] });
}