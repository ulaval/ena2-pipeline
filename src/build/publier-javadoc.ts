import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import { doExecute, doExecuteWithStdout } from "../pipeline-utils.ts";
import { copyRecursivelyToBucket } from "../aws/s3.ts";
import { AwsConfig } from "../aws/awsconfig.ts";

const { endpointUrlS3, accessKeyIdS3, accessKeySecretS3, nomService, validerJavadoc} = parse(Deno.args, {
    boolean: ["validerJavadoc"],
    default: {
        endpointUrlS3: "https://s3.dev.brioeducation.ca",
        validerJavadoc: false
    }
});

const currentWorkingDirectory = Deno.cwd();
console.log("currentWorkingDirectory: " + currentWorkingDirectory);

if (!accessKeyIdS3 || !accessKeySecretS3) {
    throw new Error("Les credentials S3 sont obligatoires");
}

await doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});


const branchName = await doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});

// Si pas sur develop, on ne permets pas la publication, seulement la validation
if (branchName != "develop" && !validerJavadoc) {
    console.warn("La publication de la javadoc se fait uniquement sur develop.");
    Deno.exit(0);
}

// Construire le site maven avec la javadoc
await doExecute("bash", {
    args: ["-c", `mvn clean -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn site -f pom.xml $MAVEN_CONFIG`]
});

// Sur develop, copie la javadoc sur S3 
if (branchName == "develop") {
    const configAWS: AwsConfig = {
        accessKeyId: accessKeyIdS3,
        accessKeySecret: accessKeySecretS3,
        region: 'ca-central-1',
        url: endpointUrlS3
    };

    copyRecursivelyToBucket(configAWS, `ena2-javadoc/${nomService}`, `${currentWorkingDirectory}/target/site`);
}
