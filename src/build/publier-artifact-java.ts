/**
 * Script qui permet de publier un artefact java (maven) si nécessaire
 *
 * Le numéro de version de l'artifact publié peut être spécifié avec le paramètre numeroVersion, sinon il sera calculé automatiquement
 * Ex. deno run --allow-run scripts/publier-api.ts --artifactoryUsername=${username} --artifactoryPassword=${mot de passe encrypté} --numeroVersion=${optionel}
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as utils from "../pipeline-utils.ts";
import * as artifactory from "../artifactory.ts";
import { tag } from "../git.ts";

const { artefacts, repoArtifact, artifactoryUtilisateur, artifactoryMotDePasse,
        buildCommit, previousSuccesfulBuildCommit, gitCredentials, gitUtilisateur, gitCourriel, repoGit,
        numeroVersion, forcerPublication, fichiersSurveilles } = parse(Deno.args, {
    boolean: ["forcerPublication"]
});

const currentWorkingDirectory = Deno.cwd();
console.log("currentWorkingDirectory: " + currentWorkingDirectory);

await utils.doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});


const branchName = await utils.doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});

if (!artefacts || !repoArtifact) {
    throw new Error("Les infos sur les artifacts à publier sont obligatoires");
}

if (!artifactoryUtilisateur || !artifactoryMotDePasse) {
    throw new Error("Les credentials artifactory sont obligatoires");
}

if (!gitCredentials || !repoGit || !gitUtilisateur || !gitCourriel) {
    throw new Error("Les infos pour le tag git sont obligatoires");
}

console.log("branchName: " + branchName);
console.log("buildCommit : " + buildCommit);
console.log("previousSuccesfulBuildCommit : " + previousSuccesfulBuildCommit);

const repertoiresSourcesModifies = await utils.doExecuteWithStdout("bash", {
    args: ["-c", "git diff --name-only " + buildCommit + " " + previousSuccesfulBuildCommit + " | awk '{split($0,a,\"/\"); print a[1];}'| sort -u | uniq"]
});

const envName = utils.getEnvName(branchName);

/**
 * Publication de la prochaine version d'un artifact:
 * Pour la branche develop:
 *      - Le code des fichiers/répertoires surveillés sont modifié.  Le numéro de version mineure est incrémentée automatiquement.
 *      - Un numéro de version peut être spécifié en paramètre de la tâche jenkins
 * Pour les autres branches :
 *       - Une version de test peut être publiée en cochant le paramètre jenkins : PUBLIER_API (forcerPublication).
 */
let versionPublication = numeroVersion ?? null;

const fichiersSurveillesArray: string[] = fichiersSurveilles ? fichiersSurveilles.split(",") : [];
let artifactModifie = false;
fichiersSurveillesArray.forEach((fichierSurveille) => {
    if (repertoiresSourcesModifies.includes(fichierSurveille)) {
        artifactModifie = true;
    }
});

const nomArtefacts: string[] = artefacts ? artefacts.split(",") : [];
if (branchName == "develop" && artifactModifie) {
    // Calculer le prochain numéro de version s'il n'est pas défini
    if (!versionPublication || versionPublication.trim().length === 0) {
        const versions = await artifactory.obtenirVersionsExistantes(`ca/ulaval/ena2/${repoArtifact}`, nomArtefacts[0], artifactory.TypeArtifact.Jar, artifactoryUtilisateur, artifactoryMotDePasse);
        versionPublication = artifactory.nouvelleVersionSemVer(versions);
    }
}

// Autres branches: déterminer le numéro de version de test si nécessaire
if (forcerPublication && branchName != "develop") {
    const commitCourt = await utils.doExecuteWithStdout("git", {
        args: ["rev-parse", "--short", "HEAD"]
    });
    versionPublication = "1.0.0-" + envName + "-" + commitCourt;
}

if (!versionPublication || versionPublication.trim().length === 0) {
    console.warn("Artifact non publié, aucune version fournie");
    Deno.exit(0);
}
console.log(`Publication de ${repoArtifact} à la version ${versionPublication}`);

// Création et push du tag (seulement sur develop)
if (branchName == "develop") {
    tag({repo: repoGit, credentials: gitCredentials, rootDir: currentWorkingDirectory, nom: gitUtilisateur, courriel: gitCourriel},
        `${versionPublication}`,
        `${versionPublication}`)
}

// Changer la version du pom
await utils.doExecute("bash", {
    args: ["-c", "mvn -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn  versions:set -DnewVersion=\"" + versionPublication + "\"  -f pom.xml $MAVEN_CONFIG"]
});

// Builder et publier le pom-parent
await utils.doExecute("bash", {
    args: ["-c", `mvn clean  -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn source:jar deploy -N -DskipTests -f pom.xml $MAVEN_CONFIG`]
});

// Les artefacts doivent être sync pour permettre au build d'attendre la fin de la publication d'un artefact dépendant (ex: communs dépend d'API)
nomArtefacts.forEach((nomArtefact:string) => {
    console.log(`Publication de l'artefact ${nomArtefact} à la version ${versionPublication}`);
    console.log(`mvn clean -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn javadoc:jar source:jar deploy -pl ${nomArtefact} -DskipTests -f pom.xml $MAVEN_CONFIG`);
    // Builder et publier la version de l'artifact
    utils.doExecuteSync("bash", {
        args: ["-c", `mvn clean -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn javadoc:jar source:jar deploy -pl ${nomArtefact} -DskipTests -f pom.xml $MAVEN_CONFIG`]
    });
});