/** Script qui permet de déployer l'app sur Openshift
 * Ex. deno run --allow-read --allow-run scripts/deploy.ts --token=$(oc whoami -t) --server=https://api.ul-pca-pr-ul02.ulaval.ca:6443 --namespace=ul-ena2-dv01
 *
 */

import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import * as oc from "../openshift.ts";
import { ConfigOpenshift } from "../openshift.ts";
import * as oracle from "../oracle/oracle.ts";
import { ServiceDetails, getDetailsServiceCourant } from "../environnement/services.ts";
import { getNomEnvironnement } from "../environnement/env-utils.ts";


const { server, token, namespace, mavenProfil, pomFilePath, nomService, avecMigrationsBD, extraConfigmapParams, activerTransfertLogs, modeDeploiement } = parse(Deno.args, {
    boolean: ["avecMigrationsBD", "activerTransfertLogs"],
    default: {
        server: "https://api.ul-pca-pr-ul02.ulaval.ca:6443",
        namespace: "ul-ena2-dv01",
        avecMigrationsBD: true,
        activerTransfertLogs: false,
        extraConfigmapParams: "",
        modeDeploiement: 'RIEN'
    }
})

// Valider les paramètre
if (modeDeploiement === 'RIEN') {
    // On ne devrait pas se trouver ici pour un build sans déploiement, mais on valide en cas d'erreur dans le Jenkinsfile qui appel.
    throw new Error("Le build est demandé sans déploiement. Aucun déploiement ne sera fait.");
}

if (!token) {
    throw new Error("Le paramètre token est obligatoire");
}

if (avecMigrationsBD && (!mavenProfil || !pomFilePath)) {
    throw new Error("Les config maven sont obligatoires");
}

if (!nomService) {
    throw new Error("Le nom du services à déployer est obligatoire");
}

const envName = await getNomEnvironnement();
const nomSchemaOracle = oracle.genererNomSchemaOracle(envName);
const envNameSansTiret = envName.replace('-', '.');
const branchePrincipale = ['main', 'develop', 'master'].some(x => x == envName);
const cloudwatchFlag = (activerTransfertLogs || branchePrincipale) ? "ul-ena2-npr" : "";
console.log("Env: " + envName + ", schéma: " + nomSchemaOracle);
console.log("Appliquer migrations bd?: " + avecMigrationsBD);
console.log("Extra params: " + extraConfigmapParams);
console.log("Condition cloudwatch: " + (activerTransfertLogs || branchePrincipale));

const configOC: ConfigOpenshift = { server: server, namespace: namespace, token: token };

const labelsConfigMap = new Map<string, string>([
    ["app", envName]
]);

const paramsConfigMap = new Map<string, string>([
    ['PALIER_NOM_ENV', 'dv.' + envNameSansTiret],
    ["NOM_ENVIRONNEMENT", envName],
    ["NOM_APPLICATION", nomService]
]);

//Traiter les params de configmap supplémentaires
const paramsKeyValues: string[] = extraConfigmapParams ? extraConfigmapParams.split(",") : [];

paramsKeyValues.forEach((paramKeyValue) => {
    const keyValue: string[] = paramKeyValue.split("=");
    paramsConfigMap.set(keyValue[0], keyValue[1]);
});

paramsConfigMap.set('NOM_SCHEMA_ORACLE', nomSchemaOracle);
if (avecMigrationsBD) {
    //Appliquer migrations bd
    await oracle.appliquerMigrationsBD(nomSchemaOracle, pomFilePath, mavenProfil);
}

//Exception pour les branches principales
if (branchePrincipale) {
    console.log("Mode monotopic inactif");
    paramsConfigMap.set('KAFKA_MODE_MONOTOPIC_ACTIF', 'false');
}
console.log("Configmap params: " + [...paramsConfigMap.entries()]);

// Process du template configmap et apply
await oc.apply(configOC, `openshift/templates/dv/template-configmap.yaml`, labelsConfigMap, paramsConfigMap);

const detailsService: ServiceDetails[] = getDetailsServiceCourant(nomService, modeDeploiement);
const nbReplicas = envName == 'develop' ? 2 : 1;

detailsService.forEach(async (service) => {
    console.log(`Déploiement du service: ${service.nomService}`)
    const labelsApp = new Map<string, string>([
        ["app", envName]
    ]);

    const paramsApp = new Map<string, string>([
        ["NOM_ENVIRONNEMENT", envName],
        ["TAG_IMAGE", envName],
        ['PROFILES_SPRING', `-Dspring.profiles.active=${service.profilsSpring}`],
        ["FLAG_CLOUDWATCH", cloudwatchFlag],
        ["NOMBRE_REPLICAS", nbReplicas]
    ]);

    const nomTemplate = service.nomTemplate === undefined ? "template-application.yaml" : service.nomTemplate;
    // Process du template application et apply
    await oc.apply(configOC, `openshift/templates/dv/${nomTemplate}`, labelsApp, paramsApp);

    //Rollout du deploymentConfig
    await oc.rolloutLatestAndWatch(configOC, `${envName}-${service.nomService}`);
});
