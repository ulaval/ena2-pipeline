import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import { doExecute, doExecuteWithStdout, getEnvName } from "../pipeline-utils.ts";
import * as oracle from "../oracle/oracle.ts";
import { getNomEnvironnement } from "../environnement/env-utils.ts";

const { mavenProfil, oauthClientId, oauthClientSecret, oauthPassword, testsUnitairesSeulement } = parse(Deno.args, {});


if (!mavenProfil || !oauthClientId || !oauthClientSecret || !oauthPassword) {
    throw new Error("Le profil maven ainsi que les params oauth sont obligatoires");
}

const envName = await getNomEnvironnement();
const paramsBDMaven = genererParametreBDMaven(envName);
const paramsOAuth2 = genererParametresOauth("password", oauthClientId, oauthClientSecret, oauthPassword);
const profilesTests = testsUnitairesSeulement == "true" ? "testsUnitairesSeulement" : "testsToutSaufE2e";
console.log("Flag testsUnitairesSeulement:" + testsUnitairesSeulement);
console.log("profilesTests:" + profilesTests);
console.log("paramsBDMaven:" + paramsBDMaven);

await doExecute("bash", {
    args: ["-c",
        `mvn test -P${mavenProfil},${profilesTests} -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn $MAVEN_CONFIG ${paramsBDMaven} ${paramsOAuth2}`]
});

if (!testsUnitairesSeulement) {
    // Hack pour rouler séparément les tests E2E des tests BD / DAO lorsqu'on veut rouler tous les tests
    await doExecute("bash", {
        args: ["-c",
            `mvn test -P${mavenProfil},testsE2e -Dtests.e2e=true -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn $MAVEN_CONFIG ${paramsBDMaven} ${paramsOAuth2}`
        ]
    })
}


export function genererParametreBDMaven(envName: string): string {
    const nomSchemaOracle = oracle.genererNomSchemaOracle(envName);
    const envNameSansTiret = envName.replace('-', '.');

    return "-Dena2-bd.usager=" + nomSchemaOracle +
        " -Dena2-bd.config=dev-jenkins" +
        " -Dena2-bd.schema=" + nomSchemaOracle +
        " -Dena2-bd.tests.usager=" + nomSchemaOracle +
        " -Dena2-bd.tests.schema=" + nomSchemaOracle +
        " -Dena2.tests.nomEnvironnement=ts." + envNameSansTiret;
}

export function genererParametresOauth(oauthGrantType: string,
    oauthClientId: string,
    oauthClientSecret: string,
    oauthPassword: string): string {
    return " -Dena2-oauth2.tests.grantType=" + oauthGrantType +
        " -Dena2-oauth2.tests.clientId=" + oauthClientId +
        " -Dena2-oauth2.tests.clientSecret=" + oauthClientSecret +
        " -Dena2-oauth2.tests.password=" + oauthPassword;
}