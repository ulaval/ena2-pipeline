/** Script qui permet de builder ena2-lti ainsi que l'image docker contenant le jar
 * Ex. deno run --allow-run scripts/build-app.ts --token=$(oc whoami -t) --server=https://api.ul-pca-pr-ul02.ulaval.ca:6443 --namespace=ul-ena2-dv01
 */
import { parse } from "https://deno.land/std@0.106.0/flags/mod.ts";
import { doExecute, doExecuteWithStdout, getEnvName } from "../pipeline-utils.ts";
import * as artifactory from "../artifactory.ts";
import { TypeArtifact } from "../artifactory.ts";
import { ConfigOpenshift } from "../openshift.ts";
import * as docker from "../docker.ts";
import * as oc from "../openshift.ts";


const { server, token, namespace, artifactoryUtilisateur, artifactoryMotDePasse, nomService, paliersDeploiement } = parse(Deno.args, {
    default: {
        server: "https://api.ul-pca-pr-ul02.ulaval.ca:6443",
        namespace: "ul-ena2-dv01",
        paliersDeploiement: "ap,ap02,pr"
    }
})

if (!nomService) {
    throw new Error("Le nom du service à builder est obligatoire");
}

const nomApplication = `ena2-${nomService}`;
const nomImage = `docker-local.maven.ulaval.ca/enseignement/ena2/${nomApplication}`;

const currentWorkingDirectory = Deno.cwd();
await doExecute("git", {
    args: ["config", "--global", "--add", "safe.directory", currentWorkingDirectory]
});

const branchName = await doExecuteWithStdout("git", {
    args: ["branch", "--show-current"]
});

const envName = getEnvName(branchName);

// Build sans tests
await doExecute("bash", {
    args: ["-c", "mvn clean -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -Dmaven.test.skip=true install -U $MAVEN_CONFIG"]
});

await docker.build(nomImage, envName, "openshift/Dockerfile");
await docker.push(nomImage, envName);

// Construire les images de déploiement de tests
if (branchName == 'develop') {
    //Construire image deploy dev
    const buildArg = new Map<string, string>([
        ["PATTERN_GABARITS", `openshift/templates/dv/*.yaml`],
    ]);
    await docker.build(`${nomImage}-deploy-dv`, 'latest', "openshift/Dockerfile-dv-gradation", buildArg);
    await docker.push(`${nomImage}-deploy-dv`, 'latest');
}

// Construire les images de déploiement de la release
if (branchName == 'main') {

    if (!artifactoryUtilisateur || !artifactoryMotDePasse) {
        throw new Error("Les credentials artifactory sont obligatoires");
    }

    // Valider les paramètre
    if (!token) {
        throw new Error("Le paramètre token est obligatoire");
    }

    const configOC: ConfigOpenshift = { server: server, namespace: namespace, token: token };
    const versions = await artifactory.obtenirVersionsExistantes("enseignement/ena2", nomApplication, TypeArtifact.Docker, artifactoryUtilisateur, artifactoryMotDePasse);
    const prochaineVersion = await artifactory.nouvelleVersionImageDocker(versions);

    //On ajoute le tag "style release"  (22.01.01-1) à l'image de la branche main
    await docker.tag(nomImage, envName, prochaineVersion);
    await docker.push(nomImage, prochaineVersion);

    const paliers: string[] = paliersDeploiement ? paliersDeploiement.split(",") : [];

    //Construire les images de déploiement
    paliers.forEach((palier) => {
        console.log(`Construction des images pour le palier ${palier}`);
        buildDeployReleaseImages(configOC, palier, prochaineVersion, nomImage, artifactoryUtilisateur, artifactoryMotDePasse);
    });

}

async function buildDeployReleaseImages(configOC: oc.ConfigOpenshift, palier: string, prochaineVersion: string, nomImage: string, artifactoryUtilisateur: string, artifactoryMotDePasse: string) {

    const paramsApp = new Map<string, string>([
        ["VERSION", prochaineVersion]
    ]);

    await oc.process(configOC, `openshift/templates/${palier}/template-configmap.yaml`, `openshift/templates/${palier}/configmap-web.json`, new Map<string, string>([]));
    await oc.process(configOC, `openshift/templates/${palier}/template-application.yaml`, `openshift/templates/${palier}/application-web.json`, paramsApp);

    await doExecute("ls", {
        args: ["-l", "openshift/templates/ap/"]
    });

    if (await isProfileTaches(palier)) {
        await oc.process(configOC, `openshift/templates/${palier}/template-configmap-taches.yaml`, `openshift/templates/${palier}/configmap-taches.json`, new Map<string, string>([]));
        await oc.process(configOC, `openshift/templates/${palier}/template-application-taches.yaml`, `openshift/templates/${palier}/application-taches.json`, paramsApp);
    }

    const buildArg = new Map<string, string>([
        ["PALIER", palier],
        ["NOM_UTILISATEUR_ARTIFACTORY", artifactoryUtilisateur],
        ["MOT_DE_PASSE_ARTIFACTORY", artifactoryMotDePasse]
    ]);

    await docker.build(`${nomImage}-deploy-${palier}`, prochaineVersion, "openshift/Dockerfile-gradation", buildArg);
    await docker.push(`${nomImage}-deploy-${palier}`, prochaineVersion);
}


/**
 * Retourne vrai si un fichier template-application-taches.yaml existe 
 */
async function isProfileTaches(palier: string): Promise<boolean> {
    const versionExiste = await doExecuteWithStdout('find', {
        args: [`./openshift/templates/${palier}/`, "-name", "template-application-taches.yaml"]
    });

    return !!versionExiste;
}



