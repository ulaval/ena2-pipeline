import { format } from 'https://deno.land/std@0.106.0/datetime/mod.ts';
import { getJson, doExecute, doExecuteWithStdout } from './pipeline-utils.ts';
import { dirname } from "https://deno.land/std@0.106.0/path/mod.ts";

/**
 * Classe qui supporte les opérations sur les fichiers d'openapi
 */
export class Openapi {
    readonly typeFichiers: string[] = ['json', 'yaml'];

    //nomFichier
    constructor(private nom: string, private pathOpenApi: string) { }

    /**
     * Retourne la version du fichier openapi-${this.nom}.json
     */
    async obtenirVersionCourante(): Promise<string> {
        //Trouver la version de l'api
        const openApiJson = await getJson(`./${this.pathOpenApi}/openapi-${this.nom}.json`);
        const version: string = openApiJson.info.version;
        return version;
    }

    /**
     * Retourne la version courante sous le format -v${version}-d${nowDate}
     */
    async obtenirVersionComplete(): Promise<string> {
        const version = await this.obtenirVersionCourante();
        const nowDate = format(new Date(), 'yyyy-MM-dd');
        const versionComplete = `-v${version}-d${nowDate}`;
        return versionComplete;
    }

    /**
     * Retourne vrai si un fichier json ou yaml existe déjà pour la version courante de l'API
     */
    async versionExiste(): Promise<boolean> {
        const version = await this.obtenirVersionCourante();
        //On cherche si la meme version de l'api existe déjà
        const versionExiste = await doExecuteWithStdout("find", {
            args: [
                `./${this.pathOpenApi}/version/`,
                '-name',
                `openapi-${this.nom}-v${version}*`,
            ],
        });

        return !!versionExiste;
    }

    /**
     * Crée des fichiers json et yaml pour la version courante de l'API
     */
    async versionner() {
        const versionComplete = await this.obtenirVersionComplete();
        console.log(`Création des versions openapi-${this.nom}${versionComplete}`);
        this.typeFichiers.forEach(async (type) =>
            await doExecute("cp", {
                args: [
                    `./${this.pathOpenApi}/openapi-${this.nom}.${type}`,
                    `./${this.pathOpenApi}/version/openapi-${this.nom}${versionComplete}.${type}`,
                ],
            })
        );
    }

    /**
     * Génère le changelog entre la version courante et la version précédente de l'API
     */
    async genererChangeLog() {
        const versionComplete = await this.obtenirVersionComplete();
        const versionCourante = await this.obtenirVersionCourante();
        console.log(`Génération du changelog`);
        const changeLog = await doExecuteWithStdout('oasdiff', {
            args: [
                '-format',
                'html',
                '-base',
                `${this.pathOpenApi}/version/openapi-${this.nom}-version-precedente.yaml`,
                '-revision',
                `./${this.pathOpenApi}/version/openapi-${this.nom}${versionComplete}.yaml`,
            ],
        });
        const filename = `./${this.pathOpenApi}/changelog/openapi-${this.nom}-changelog-v${versionCourante}.html`;
        await Deno.writeTextFile(filename, changeLog);
    }

    /**
     * Fait une copie de la version courante vers la version precedente
     */
    async archiver() {
        const versionComplete = await this.obtenirVersionComplete();
        console.log(`Copier l'api vers le fichier version-precedente`);
        this.typeFichiers.forEach(async (type) =>
            await doExecute('cp', {
                args: [
                    `./${this.pathOpenApi}/version/openapi-${this.nom}${versionComplete}.${type}`,
                    `./${this.pathOpenApi}/version/openapi-${this.nom}-version-precedente.${type}`,
                ],
            })
        );
    }

    /**
     * Sauvegarde les fichiers générés et modifiés dans le repo GIT d'ena2-modules
     * @param gitUser
     * @param gitPassword
     * @param gitEmail
     */
    async sauvegarder(gitUser: string, gitPassword: string, gitEmail: string) {
        const versionCourante = await this.obtenirVersionCourante();
        const nowDate = format(new Date(), 'yyyy-MM-dd');
        const gitCred = `${gitUser}:${gitPassword}`;

        const currentWorkingDirectory = Deno.cwd();
        // On change le workdir pour celui de ena2-modules
        const ena2ModulesDir = dirname(currentWorkingDirectory); // Remonte d'un niveau de répertoire

        console.log(`currentWorkingDirectory: ${currentWorkingDirectory}`);
        console.log(`ena2-modules dir: ${ena2ModulesDir}`);


        await doExecute("git", {
            args: ["config", "--global", "--add", "safe.directory", ena2ModulesDir]
        });

        await doExecute("git", { args: ['add', '*/openapi-*.html'] });
        await doExecute("git", { args: ['add', '*/openapi-*.yaml'] });
        await doExecute("git", { args: ['add', '*/openapi-*.json'] });

        await doExecute('git', {
            args: [
                '-c',
                `user.name='${gitUser}'`,
                '-c',
                `user.email='${gitEmail}'`,
                'commit',
                '-am',
                `Création ${this.nom} openapi version ${versionCourante} datée du ${nowDate}`,
            ],
        });

        const gitRepositoryBE = `https://${gitCred}@github.com/universite-laval/ul-ena2-modules.git`;
        await doExecute("git", { args: ['push', gitRepositoryBE] });
    }
}
