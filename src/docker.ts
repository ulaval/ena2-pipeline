import * as utils from './pipeline-utils.ts';
import * as oc from './openshift.ts';

export async function build(nomImage: string, tag: string, dockerfilePath: string, buildArgsMap: Map<string, string> = new Map()) {

    if (!nomImage || !tag) {
        throw new Error("Les parametres nomImage et tag sont obligatoires");
    }
    const buildArgs = construireBuildParams(buildArgsMap);
    const paramBuildArgs = buildArgs ? buildArgs : ``;

    console.log(`docker build . --tag ${nomImage}:${tag} --file=${dockerfilePath} ${paramBuildArgs}`);

    await utils.doRun({
        cmd: ['bash',
            '-c',
            "docker build . --tag " +
            `${nomImage}:${tag} ` +
            `--file=${dockerfilePath} ` +
            `${paramBuildArgs}`
        ]
    });

}

export async function tag(nomImage: string, tagSource: string, tagDestination: string) {
    console.log(`docker tag ${nomImage}:${tagSource} ${nomImage}:${tagDestination}`);
    await utils.doRun({
        cmd: ["docker",
            "tag",
            `${nomImage}:${tagSource}`,
            `${nomImage}:${tagDestination}`
        ]
    });
}

export async function push(nomImage: string, tag: string) {

    console.log(`docker push ${nomImage}:${tag}`);
    await utils.doRun({
        cmd: ["docker",
            "push",
            `${nomImage}:${tag}`
        ]
    });

}

function construireBuildParams(mapLabels: Map<string, string>): string {
    const listeLabels: string[] = [];
    mapLabels.forEach((value, key) => {
        listeLabels.push(`--build-arg ${key}=${value}`);
    });

    return listeLabels.join(' ');
}

//TODO Mettre à jour ena-identite pour enlever ces fonctions
export async function buildReleaseImage(rootDir: string, envName: string, version: string) {
    // Tag master image with release version
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'docker',
            'image',
            'tag',
            `docker-local.maven.ulaval.ca/enseignement/ena/ena-identite:${envName}`,
            `docker-local.maven.ulaval.ca/enseignement/ena/ena-identite:${version}`,
        ],
    });

    // Push docker image
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'docker',
            'push',
            `docker-local.maven.ulaval.ca/enseignement/ena/ena-identite:${version}`,
        ],
    });
}

//TODO Mettre à jour ena-identite pour enlever ces fonctions
export async function buildDeployImages(server: string, token: string, namespace: string, rootDir: string, palier: string, version: string) {
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'bash',
            '-c',
            'oc process ' +
            `--server=${server} ` +
            `--token=${token} ` +
            `--namespace=${namespace} ` +
            `-f scripts/templates/${palier}/template-configmap.yaml ` +
            `> scripts/templates/${palier}/configmap-web.json`,
        ],
    });

    // Process template application
    let applicationTemplateParameters = `-p=VERSION=${version} `;
    if (['dv', 'dvpr', 'ts', 'tspr'].includes(palier)) {
        applicationTemplateParameters = `-p=NOM_ENVIRONNEMENT=${version} `;
    }
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'bash',
            '-c',
            'oc process ' +
            `--server=${server} ` +
            `--token=${token} ` +
            `--namespace=${namespace} ` +
            applicationTemplateParameters +
            `-f scripts/templates/${palier}/template-application.yaml ` +
            `> scripts/templates/${palier}/application-web.json`,
        ],
    });

    // Build docker
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'docker',
            'build',
            '.',
            '--tag',
            `docker-local.maven.ulaval.ca/enseignement/ena/ena-identite-deploy-${palier}:${version}`,
            '--build-arg',
            `PATTERN_GABARITS=scripts/templates/${palier}/*.json`,
            '--file=scripts/Dockerfile-gradation',
        ],
    });

    // Push docker
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'docker',
            'push',
            `docker-local.maven.ulaval.ca/enseignement/ena/ena-identite-deploy-${palier}:${version}`,
        ],
    });
}

export async function buildBrioDevDeployImages(rootDir: string, module: string, version: string) {
    // Build docker
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'docker',
            'build',
            '.',
            '--tag',
            `docker-local.maven.ulaval.ca/enseignement/ena2/${module}-deploy-dv:${version}`,
            '--build-arg',
            `PATTERN_GABARITS=ena2-modules/${module}/openshift/templates/dv/*.yaml`,
            '--file=./Dockerfile-gradation',
        ],
    });

    // Push docker
    console.log('Skipping docker push');
    await utils.doRun({
        cwd: rootDir,
        cmd: [
            'docker',
            'push',
            `docker-local.maven.ulaval.ca/enseignement/ena2/${module}-deploy-dv:${version}`,
        ],
    });
}


export async function buildDeployReleaseImages(configOC: oc.ConfigOpenshift, palier: string, prochaineVersion: string, nomImage: string, artifactoryUtilisateur: string, artifactoryMotDePasse: string) {

    const paramsApp = new Map<string, string>([
        ["VERSION", prochaineVersion]
    ]);

    oc.process(configOC, `openshift/templates/${palier}/template-configmap.yaml`, `openshift/templates/${palier}/configmap-web.json`, new Map<string, string>([]));
    oc.process(configOC, `openshift/templates/${palier}/template-application.yaml`, `openshift/templates/${palier}/application-web.json`, paramsApp);

    const buildArg = new Map<string, string>([
        ["PATTERN_GABARITS", `openshift/templates/${palier}/*.json`],
        ["NOM_UTILISATEUR_ARTIFACTORY", artifactoryUtilisateur],
        ["MOT_DE_PASSE_ARTIFACTORY", artifactoryMotDePasse]
    ]);

    await build(`${nomImage}-deploy-${palier}`, prochaineVersion, "openshift/Dockerfile-gradation", buildArg);
    await push(`${nomImage}-deploy-${palier}`, prochaineVersion);
}
