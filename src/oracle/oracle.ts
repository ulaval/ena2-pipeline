import * as utils from "../pipeline-utils.ts";
import { OracleConfig } from "./oracleConfig.ts";

/** Génère le schéma oracle à partir du nom de la branche git*/
export async function creerSchemaOracle(
  nomSchema: string,
  config: OracleConfig,
) {
  const urlBD =
    (config.nomUtilisateur + "/" + config.motDePasse + "@" + config.hoteOracle +
      ":" + config.portOracle + "/" +
      config.serviceOracle).trim();

  await utils.doExecute("docker", {
    args: [
      "run",
      "--rm",
      "-e",
      "URL=" + urlBD,
      "-t",
      "docker-local.maven.ulaval.ca/enseignement/ena2/ena2-sqlplus:23.03.23-2",
      "bash",
      "/oracle-schema-management.sh",
      "create",
      nomSchema,
    ],
  });
}

export async function schemaOracleExiste(
  nomSchema: string,
  config: OracleConfig,
): Promise<boolean> {
  const urlBD =
    (config.nomUtilisateur + "/" + config.motDePasse + "@" + config.hoteOracle +
      ":" + config.portOracle + "/" +
      config.serviceOracle).trim();
  const result = await utils.doExecuteWithStdout("docker",  {
    args: [
      "run",
      "--rm",
      "-e",
      "URL=" + urlBD,
      "-t",
      "docker-local.maven.ulaval.ca/enseignement/ena2/ena2-sqlplus:23.03.23-2",
      "bash",
      "/oracle-schema-management.sh",
      "exist",
      nomSchema,
    ],
  });

  return result.includes(nomSchema);
}

export async function initBD(oracleConfig: OracleConfig, nomEnvironnement: string, mavenSettingsPath: string, mavenProfil: string, rechargerDonnees: boolean) {
  const nomSchema = genererNomSchemaOracle(nomEnvironnement);
  const schemaExiste = await schemaOracleExiste(nomSchema, oracleConfig);
  console.log(`Schema oracle ${nomSchema} existe? : ${schemaExiste}`);

  //creation schema 
  if (!schemaExiste) {
    await creerSchemaOracle(nomSchema, oracleConfig);
  }

  if (!schemaExiste || rechargerDonnees) {
    await utils.doExecute('docker', {
      args: [
        'run',
        '--pull=always',
        '-v',
        `${mavenSettingsPath}:/settings.xml:z`,
        '-t',
        'docker-local.maven.ulaval.ca/enseignement/ena2/ena2-bd-migrations-dv:latest',
        'bash',
        '-c',
        "mvn --batch-mode -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn --errors --offline --file /ena2-modules/ena2-bd/pom.xml bd:rechargerTout" +
        ` --settings /settings.xml -Dmaven.repo.local=/maven/.m2 -P${mavenProfil} -Dena2-bd.usager=${nomSchema} -Dena2-bd.schema=${nomSchema} -Dena2-bd.config=dev-jenkins`

      ]
    });
  }

}

export async function appliquerMigrationsBD(nomSchema: string, pomFilePath: string, mavenProfil: string) {

  console.log("===================================================");
  console.log(`Exécuter les migrations de ${pomFilePath}`);
  console.log("===================================================");
  console.log(`Profil maven : ${mavenProfil}`)
  console.log(`Nom schema : ${nomSchema}`)
  console.log("===================================================");
  await utils.doRun({
    cmd: ["bash", "-c", `mvn -f ${pomFilePath} $MAVEN_CONFIG bd:migrations -P${mavenProfil} ` +
      `-Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=warn -Dena2-bd.usager=${nomSchema} ` +
      `-Dena2-bd.schema=${nomSchema} -Dena2-bd.config=dev-jenkins`]
  });
  console.log("===================================================");
  console.log(`Migrations de ${pomFilePath} complétées`);
  console.log("===================================================");

}

/** Génère le nom du schéma oracle à partir de la branche git*/
export function genererNomSchemaOracle(nomEnv: string): string {
  return ("ENA2_" + extraireNomSchemaOracle(nomEnv) + "_PROP").toUpperCase();
}

function extraireNomSchemaOracle(nomEnv: string): string {
  if (nomEnv == "develop" || nomEnv == "main" || nomEnv == "master") {
    return nomEnv;
  }

  const patternNomValide = new RegExp(/([a-zA-Z0-9]+)-([a-zA-Z0-9-]+)/);
  const result = patternNomValide.exec(nomEnv);

  if (result && result.length === 3) {
    return result[2];
  }

  throw new Error("Nom de schéma invalide:" + nomEnv);
}
