export interface OracleConfig {
    nomUtilisateur: string;
    motDePasse: string;
    hoteOracle: string;
    portOracle: string;
    serviceOracle: string;
}
